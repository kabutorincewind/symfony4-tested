<?php

namespace App\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Intl\Intl;

class SiteController extends AbstractController
{
    /**
     * @Route(path="/", name="homepage")
     * @return Response
     */
    function homepage()
    {
        return $this->render('index.html.twig');
    }

    /**
     * @Route(path="/getCoordinates", name="get_coordinates")
     * @return JsonResponse
     */
    function getCitiesCoords(Request $request) : JsonResponse
    {
        $ids = $request->get('ids');
        $cities = $this->getDoctrine()
            ->getRepository('App:City')->getByIds($ids);
        return new JsonResponse($cities, 200);
    }

    /**
     * @Route("/statistics", name="statistics")
     * @return Response
     */
    function statistics()
    {
        $visits = $this->getDoctrine()
            ->getRepository('App:SiteVisit')
            ->getFromLastWeek();
        $userRegistrations = $this->getDoctrine()
            ->getRepository('App:User')
            ->getRegistrationsByCity();

        return $this->render('statistics.html.twig',
            [
                'userRegistrations' => $userRegistrations,
                'visits' => $visits
            ]
        );
    }
}