<?php

namespace App\Repository;


use App\Entity\City;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityRepository;

class CityRepository extends EntityRepository
{
    /**
     * @return array visits count for every day of last week
     */
    public function getByIds($ids)
    {
        $expr = Criteria::expr();
        $qb = $this->createQueryBuilder('q')
            ->select(['q.id','q.name', 'q.latitude', 'q.longitude'])
            ->where($expr->in('q.id', $ids))
            ->orderBy('q.id')
            ->groupBy('q.name');

        return $qb->getQuery()->getResult();
    }
}