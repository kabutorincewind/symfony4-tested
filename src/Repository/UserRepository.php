<?php

namespace App\Repository;


use App\Entity\User;

class UserRepository extends \Doctrine\ORM\EntityRepository
{
    /**
     * @return array this month and total registrations grouped by city
     */
    public function getRegistrationsByCity()
    {
        $lastMonth = \DateTime::createFromFormat('U', strtotime("-1 month"));
        $qb = $this->createQueryBuilder('q')
            ->select(['c.name','count(q.email) as thisMonth', 'count(q1.email) as total'])
            ->join('App:User', 'q1', 'with', 'q1.id = q.id')
            ->join('App:City', 'c', 'with', 'c.id = q.city')
            ->where('q1.createdAt >= :lastMonth')
            ->setParameter('lastMonth', $lastMonth)
            ->groupBy('q.city')
            ->orderBy('q.city');

        return $qb->getQuery()->getResult();
    }

    public function getVisitsLastWeek()
    {

    }
}