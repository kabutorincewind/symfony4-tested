<?php

namespace App\Repository;


use App\Entity\SiteVisit;
use App\Entity\User;
use Doctrine\ORM\EntityRepository;

class SiteVisitRepository extends EntityRepository
{
    public function logUserVisit(User $user)
    {
        $visit = new SiteVisit();
        $visit->setUser($user);
        $em = $this->getEntityManager();
        $em->persist($visit);
        $em->flush();
    }
    /**
     * @return array visits count for every day of last week
     */
    public function getFromLastWeek()
    {
        $lastWeek = \DateTime::createFromFormat('U', strtotime("-6 days"));
        $qb = $this->createQueryBuilder('q')
            ->select(['date(q.createdAt) as c_date', 'count(q.id) as count'])
            ->where('q.createdAt >= :lastWeek')
            ->setParameter('lastWeek', $lastWeek)
            ->orderBy('c_date')
            ->groupBy('c_date');

        return $qb->getQuery()->getResult();
    }

    public function getVisitsLastWeek()
    {

    }
}