<?php

namespace App\Form\User;

use App\Entity\User;
use Evercode\DependentSelectBundle\Form\Type\DependentFilteredEntityType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EditType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('password', RepeatedType::class, [
                'type' => PasswordType::class,
                'first_options' => ['label' => 'user.password.first'],
                'second_options' => ['label' => 'user.password.second'],
            ])
            ->add('name', TextType::class)
            ->add('surname', TextType::class)
            ->add('country', EntityType::class,
                [
                    'class' => 'App:Country',
                    'required'   => true,
                    'placeholder'=> 'Choose country'
                ]
            )
            ->add('region', DependentFilteredEntityType::class,
                [
                    'entity_alias' => 'region_by_country',
                    'empty_value'=> 'Choose region',
                    'parent_field'=>'country'
                ]
            )
            ->add('city', DependentFilteredEntityType::class,
                [
                    'entity_alias' => 'city_by_region',
                    'empty_value'=> 'Choose city',
                    'parent_field'=>'region'
                ]
            )
            ->add('gender',  ChoiceType::class, [
                'expanded' => true,
                'multiple' => false,
                'choices'  => [
                    'Male' => true,
                    'Female' => false,
                ]
            ])
            ->add('birthday', BirthdayType::class);;
        parent::buildForm($builder, $options);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
                'data_class' => User::class,
                'validation_groups' => ['Default', 'Edit']
            ]
        );
    }
}