#### Installation

1. configure database access uri and recaptcha cred's at .ENV file
2. clone  https://github.com/prograhammer/countries-regions-cities
 and import that database into yours
     
         $ gunzip world.sql.gz
         $ mysql -u myusername -p mypassword
         mysql> use %mydatabase%;
         mysql> source world.sql;
         
3. run `composer install && npm install && npm run build`
4. run `./bin/console doctrine:schema:update --force`
5. run server by `./bin/console server:run`