<?php
require 'vendor/autoload.php';

use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\HttpKernel\Kernel;

class AppKernel extends Kernel implements KernelInterface{

    public function registerBundles() :Kernel
    {
        parent::registerBundles();
        return $this;
    }

    public function registerContainerConfiguration(\Symfony\Component\Config\Loader\LoaderInterface $loader) :Kernel
    {
        parent::registerContainerConfiguration($loader);
        return $this;
    }

    /**
     * Checks if a given class name belongs to an active bundle.
     *
     * @param string $class A class name
     *
     * @return bool true if the class belongs to an active bundle, false otherwise
     *
     * @deprecated since version 2.6, to be removed in 3.0.
     */
    public function isClassInActiveBundle($class) :bool
    {
        return parent::isClassInActiveBundle($class);
    }

};