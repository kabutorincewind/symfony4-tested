<?php

/* @WebProfiler/Profiler/open.html.twig */
class __TwigTemplate_7372a771c6ff1d7fe6f73b3ed4d7bffabf3eac325649e216811b3f1a54ad883f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/base.html.twig", "@WebProfiler/Profiler/open.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_645970e911b4619bab482bb3605ad1811cefdd15fb832cfc5abbaa283807097a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_645970e911b4619bab482bb3605ad1811cefdd15fb832cfc5abbaa283807097a->enter($__internal_645970e911b4619bab482bb3605ad1811cefdd15fb832cfc5abbaa283807097a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Profiler/open.html.twig"));

        $__internal_4c1c1ca4b60226eca31d4eb47a8d3faffda4c83c07211ca5c34d050378443f5c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4c1c1ca4b60226eca31d4eb47a8d3faffda4c83c07211ca5c34d050378443f5c->enter($__internal_4c1c1ca4b60226eca31d4eb47a8d3faffda4c83c07211ca5c34d050378443f5c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Profiler/open.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_645970e911b4619bab482bb3605ad1811cefdd15fb832cfc5abbaa283807097a->leave($__internal_645970e911b4619bab482bb3605ad1811cefdd15fb832cfc5abbaa283807097a_prof);

        
        $__internal_4c1c1ca4b60226eca31d4eb47a8d3faffda4c83c07211ca5c34d050378443f5c->leave($__internal_4c1c1ca4b60226eca31d4eb47a8d3faffda4c83c07211ca5c34d050378443f5c_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_81a246b0a6b58b2f522c9c69c694fad2fb3e2c5fc4f75178b142e02d8fb3dcb0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_81a246b0a6b58b2f522c9c69c694fad2fb3e2c5fc4f75178b142e02d8fb3dcb0->enter($__internal_81a246b0a6b58b2f522c9c69c694fad2fb3e2c5fc4f75178b142e02d8fb3dcb0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_013960b056c454ea348909dd8d90b86fb04496473ed6d5731db551baa8818eab = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_013960b056c454ea348909dd8d90b86fb04496473ed6d5731db551baa8818eab->enter($__internal_013960b056c454ea348909dd8d90b86fb04496473ed6d5731db551baa8818eab_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <style>
        ";
        // line 5
        echo twig_include($this->env, $context, "@WebProfiler/Profiler/open.css.twig");
        echo "
    </style>
";
        
        $__internal_013960b056c454ea348909dd8d90b86fb04496473ed6d5731db551baa8818eab->leave($__internal_013960b056c454ea348909dd8d90b86fb04496473ed6d5731db551baa8818eab_prof);

        
        $__internal_81a246b0a6b58b2f522c9c69c694fad2fb3e2c5fc4f75178b142e02d8fb3dcb0->leave($__internal_81a246b0a6b58b2f522c9c69c694fad2fb3e2c5fc4f75178b142e02d8fb3dcb0_prof);

    }

    // line 9
    public function block_body($context, array $blocks = array())
    {
        $__internal_3a951202040d65154d227056ae52625f558e98858566c72cae9cc1490f3e8256 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3a951202040d65154d227056ae52625f558e98858566c72cae9cc1490f3e8256->enter($__internal_3a951202040d65154d227056ae52625f558e98858566c72cae9cc1490f3e8256_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_c27c310db24fda59ed8a2b2c2f34d6243b35d1ec7a4df2934bf59c58121469fa = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c27c310db24fda59ed8a2b2c2f34d6243b35d1ec7a4df2934bf59c58121469fa->enter($__internal_c27c310db24fda59ed8a2b2c2f34d6243b35d1ec7a4df2934bf59c58121469fa_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 10
        echo "<div class=\"header\">
    <h1>";
        // line 11
        echo twig_escape_filter($this->env, (isset($context["file"]) || array_key_exists("file", $context) ? $context["file"] : (function () { throw new Twig_Error_Runtime('Variable "file" does not exist.', 11, $this->getSourceContext()); })()), "html", null, true);
        echo " <small>line ";
        echo twig_escape_filter($this->env, (isset($context["line"]) || array_key_exists("line", $context) ? $context["line"] : (function () { throw new Twig_Error_Runtime('Variable "line" does not exist.', 11, $this->getSourceContext()); })()), "html", null, true);
        echo "</small></h1>
    <a class=\"doc\" href=\"https://symfony.com/doc/";
        // line 12
        echo twig_escape_filter($this->env, twig_constant("Symfony\\Component\\HttpKernel\\Kernel::VERSION"), "html", null, true);
        echo "/reference/configuration/framework.html#ide\" rel=\"help\">Open in your IDE?</a>
</div>
<div class=\"source\">
    ";
        // line 15
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\CodeExtension')->fileExcerpt((isset($context["filename"]) || array_key_exists("filename", $context) ? $context["filename"] : (function () { throw new Twig_Error_Runtime('Variable "filename" does not exist.', 15, $this->getSourceContext()); })()), (isset($context["line"]) || array_key_exists("line", $context) ? $context["line"] : (function () { throw new Twig_Error_Runtime('Variable "line" does not exist.', 15, $this->getSourceContext()); })()),  -1);
        echo "
</div>
";
        
        $__internal_c27c310db24fda59ed8a2b2c2f34d6243b35d1ec7a4df2934bf59c58121469fa->leave($__internal_c27c310db24fda59ed8a2b2c2f34d6243b35d1ec7a4df2934bf59c58121469fa_prof);

        
        $__internal_3a951202040d65154d227056ae52625f558e98858566c72cae9cc1490f3e8256->leave($__internal_3a951202040d65154d227056ae52625f558e98858566c72cae9cc1490f3e8256_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Profiler/open.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  90 => 15,  84 => 12,  78 => 11,  75 => 10,  66 => 9,  53 => 5,  50 => 4,  41 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/base.html.twig' %}

{% block head %}
    <style>
        {{ include('@WebProfiler/Profiler/open.css.twig') }}
    </style>
{% endblock %}

{% block body %}
<div class=\"header\">
    <h1>{{ file }} <small>line {{ line }}</small></h1>
    <a class=\"doc\" href=\"https://symfony.com/doc/{{ constant('Symfony\\\\Component\\\\HttpKernel\\\\Kernel::VERSION') }}/reference/configuration/framework.html#ide\" rel=\"help\">Open in your IDE?</a>
</div>
<div class=\"source\">
    {{ filename|file_excerpt(line, -1) }}
</div>
{% endblock %}
", "@WebProfiler/Profiler/open.html.twig", "/home/sj/PhpstormProjects/symfony4-project/vendor/symfony/web-profiler-bundle/Resources/views/Profiler/open.html.twig");
    }
}
