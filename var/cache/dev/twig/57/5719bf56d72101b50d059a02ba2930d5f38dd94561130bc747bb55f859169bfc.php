<?php

/* @WebProfiler/Icon/twig.svg */
class __TwigTemplate_65b52dc976a7d8e362dc99b774b3998ac73544d150ed8070bb9ba58204cde4f6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ab75d816596c52a139a17517e1f68780c87b43cafa4d5ffbdfba584c0f9737b0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ab75d816596c52a139a17517e1f68780c87b43cafa4d5ffbdfba584c0f9737b0->enter($__internal_ab75d816596c52a139a17517e1f68780c87b43cafa4d5ffbdfba584c0f9737b0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Icon/twig.svg"));

        $__internal_95ec8b23ae2aa8a547f2b5fb70ecd7e04853649eabc0f2418a21fd5621d75d5e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_95ec8b23ae2aa8a547f2b5fb70ecd7e04853649eabc0f2418a21fd5621d75d5e->enter($__internal_95ec8b23ae2aa8a547f2b5fb70ecd7e04853649eabc0f2418a21fd5621d75d5e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Icon/twig.svg"));

        // line 1
        echo "<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" x=\"0px\" y=\"0px\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" enable-background=\"new 0 0 24 24\" xml:space=\"preserve\">
<path fill=\"#AAAAAA\" d=\"M20.1,1H3.9C2.3,1,1,2.3,1,3.9v16.3C1,21.7,2.3,23,3.9,23h16.3c1.6,0,2.9-1.3,2.9-2.9V3.9
    C23,2.3,21.7,1,20.1,1z M21,20.1c0,0.5-0.4,0.9-0.9,0.9H3.9C3.4,21,3,20.6,3,20.1V3.9C3,3.4,3.4,3,3.9,3h16.3C20.6,3,21,3.4,21,3.9
    V20.1z M5,5h14v3H5V5z M5,10h3v9H5V10z M10,10h9v9h-9V10z\"/>
</svg>
";
        
        $__internal_ab75d816596c52a139a17517e1f68780c87b43cafa4d5ffbdfba584c0f9737b0->leave($__internal_ab75d816596c52a139a17517e1f68780c87b43cafa4d5ffbdfba584c0f9737b0_prof);

        
        $__internal_95ec8b23ae2aa8a547f2b5fb70ecd7e04853649eabc0f2418a21fd5621d75d5e->leave($__internal_95ec8b23ae2aa8a547f2b5fb70ecd7e04853649eabc0f2418a21fd5621d75d5e_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Icon/twig.svg";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" x=\"0px\" y=\"0px\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" enable-background=\"new 0 0 24 24\" xml:space=\"preserve\">
<path fill=\"#AAAAAA\" d=\"M20.1,1H3.9C2.3,1,1,2.3,1,3.9v16.3C1,21.7,2.3,23,3.9,23h16.3c1.6,0,2.9-1.3,2.9-2.9V3.9
    C23,2.3,21.7,1,20.1,1z M21,20.1c0,0.5-0.4,0.9-0.9,0.9H3.9C3.4,21,3,20.6,3,20.1V3.9C3,3.4,3.4,3,3.9,3h16.3C20.6,3,21,3.4,21,3.9
    V20.1z M5,5h14v3H5V5z M5,10h3v9H5V10z M10,10h9v9h-9V10z\"/>
</svg>
", "@WebProfiler/Icon/twig.svg", "/home/sj/PhpstormProjects/symfony4-project/vendor/symfony/web-profiler-bundle/Resources/views/Icon/twig.svg");
    }
}
