<?php

/* @Twig/images/icon-plus-square.svg */
class __TwigTemplate_220b9208f72039b65aa3e3e223af97dbeacc3b12132c436ba21abc8c02a7ca7a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b3020dd149662061d593e13deb390f3115b837f672356bbc390bd6739fc52c05 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b3020dd149662061d593e13deb390f3115b837f672356bbc390bd6739fc52c05->enter($__internal_b3020dd149662061d593e13deb390f3115b837f672356bbc390bd6739fc52c05_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/images/icon-plus-square.svg"));

        $__internal_20987f34be8717712b7768ec317c12ce45da68ff7436044ba93daab046c2ad0f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_20987f34be8717712b7768ec317c12ce45da68ff7436044ba93daab046c2ad0f->enter($__internal_20987f34be8717712b7768ec317c12ce45da68ff7436044ba93daab046c2ad0f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/images/icon-plus-square.svg"));

        // line 1
        echo "<svg width=\"1792\" height=\"1792\" viewBox=\"0 0 1792 1792\" xmlns=\"http://www.w3.org/2000/svg\"><path d=\"M1408 960v-128q0-26-19-45t-45-19h-320v-320q0-26-19-45t-45-19h-128q-26 0-45 19t-19 45v320h-320q-26 0-45 19t-19 45v128q0 26 19 45t45 19h320v320q0 26 19 45t45 19h128q26 0 45-19t19-45v-320h320q26 0 45-19t19-45zm256-544v960q0 119-84.5 203.5t-203.5 84.5h-960q-119 0-203.5-84.5t-84.5-203.5v-960q0-119 84.5-203.5t203.5-84.5h960q119 0 203.5 84.5t84.5 203.5z\"/></svg>
";
        
        $__internal_b3020dd149662061d593e13deb390f3115b837f672356bbc390bd6739fc52c05->leave($__internal_b3020dd149662061d593e13deb390f3115b837f672356bbc390bd6739fc52c05_prof);

        
        $__internal_20987f34be8717712b7768ec317c12ce45da68ff7436044ba93daab046c2ad0f->leave($__internal_20987f34be8717712b7768ec317c12ce45da68ff7436044ba93daab046c2ad0f_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/images/icon-plus-square.svg";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<svg width=\"1792\" height=\"1792\" viewBox=\"0 0 1792 1792\" xmlns=\"http://www.w3.org/2000/svg\"><path d=\"M1408 960v-128q0-26-19-45t-45-19h-320v-320q0-26-19-45t-45-19h-128q-26 0-45 19t-19 45v320h-320q-26 0-45 19t-19 45v128q0 26 19 45t45 19h320v320q0 26 19 45t45 19h128q26 0 45-19t19-45v-320h320q26 0 45-19t19-45zm256-544v960q0 119-84.5 203.5t-203.5 84.5h-960q-119 0-203.5-84.5t-84.5-203.5v-960q0-119 84.5-203.5t203.5-84.5h960q119 0 203.5 84.5t84.5 203.5z\"/></svg>
", "@Twig/images/icon-plus-square.svg", "/home/sj/PhpstormProjects/symfony4-project/vendor/symfony/twig-bundle/Resources/views/images/icon-plus-square.svg");
    }
}
