<?php

/* DependentSelectBundle::fields.html.twig */
class __TwigTemplate_c2f3599a0adde75d35c79810ef7bd20a0d584081adfbf268372252407bebbf10 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'dependent_filtered_entity_widget' => array($this, 'block_dependent_filtered_entity_widget'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4aa24db9cd6c49804a51c04e6dbf29aa4da304397d57c1994dc84d3307090404 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4aa24db9cd6c49804a51c04e6dbf29aa4da304397d57c1994dc84d3307090404->enter($__internal_4aa24db9cd6c49804a51c04e6dbf29aa4da304397d57c1994dc84d3307090404_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "DependentSelectBundle::fields.html.twig"));

        $__internal_96572dc3e474b0c3203fc01c7931eadb8554e8948eac18dc3c4cff99160d8f9a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_96572dc3e474b0c3203fc01c7931eadb8554e8948eac18dc3c4cff99160d8f9a->enter($__internal_96572dc3e474b0c3203fc01c7931eadb8554e8948eac18dc3c4cff99160d8f9a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "DependentSelectBundle::fields.html.twig"));

        // line 1
        $this->displayBlock('dependent_filtered_entity_widget', $context, $blocks);
        
        $__internal_4aa24db9cd6c49804a51c04e6dbf29aa4da304397d57c1994dc84d3307090404->leave($__internal_4aa24db9cd6c49804a51c04e6dbf29aa4da304397d57c1994dc84d3307090404_prof);

        
        $__internal_96572dc3e474b0c3203fc01c7931eadb8554e8948eac18dc3c4cff99160d8f9a->leave($__internal_96572dc3e474b0c3203fc01c7931eadb8554e8948eac18dc3c4cff99160d8f9a_prof);

    }

    public function block_dependent_filtered_entity_widget($context, array $blocks = array())
    {
        $__internal_32d5764d17c08d19812d8697f75d4b43c193e9f26c9ea6b513d06dffadca87ba = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_32d5764d17c08d19812d8697f75d4b43c193e9f26c9ea6b513d06dffadca87ba->enter($__internal_32d5764d17c08d19812d8697f75d4b43c193e9f26c9ea6b513d06dffadca87ba_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "dependent_filtered_entity_widget"));

        $__internal_5809b77e6fbc31fbcd688402f51fbe91521641b916d20c072355b762e1e1628c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5809b77e6fbc31fbcd688402f51fbe91521641b916d20c072355b762e1e1628c->enter($__internal_5809b77e6fbc31fbcd688402f51fbe91521641b916d20c072355b762e1e1628c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "dependent_filtered_entity_widget"));

        // line 2
        echo "
    <select ";
        // line 3
        $this->displayBlock("widget_attributes", $context, $blocks);
        echo " class=\"form-control\" disabled></select>

    <script type=\"text/javascript\">
        jQuery(function(){

            jQuery(\"select#";
        // line 8
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 8, $this->getSourceContext()); })()), "parent", array()), "offsetGet", array(0 => (isset($context["parent_field"]) || array_key_exists("parent_field", $context) ? $context["parent_field"] : (function () { throw new Twig_Error_Runtime('Variable "parent_field" does not exist.', 8, $this->getSourceContext()); })())), "method"), "vars", array()), "id", array()), "html", null, true);
        echo "\").change( function() {
                var selected_index = ";
        // line 9
        echo twig_escape_filter($this->env, (((isset($context["value"]) || array_key_exists("value", $context) ? $context["value"] : (function () { throw new Twig_Error_Runtime('Variable "value" does not exist.', 9, $this->getSourceContext()); })())) ? ((isset($context["value"]) || array_key_exists("value", $context) ? $context["value"] : (function () { throw new Twig_Error_Runtime('Variable "value" does not exist.', 9, $this->getSourceContext()); })())) : (0)), "html", null, true);
        echo ";
                jQuery(\"select#";
        // line 10
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 10, $this->getSourceContext()); })()), "vars", array()), "id", array()), "html", null, true);
        echo "\").get(0).disabled = true;
                jQuery.ajax({
                    type: \"POST\",
                    data: {
                        parent_id: jQuery(this).val(),
                        ";
        // line 15
        if ( !(null === (isset($context["fallback_parent_field"]) || array_key_exists("fallback_parent_field", $context) ? $context["fallback_parent_field"] : (function () { throw new Twig_Error_Runtime('Variable "fallback_parent_field" does not exist.', 15, $this->getSourceContext()); })()))) {
            // line 16
            echo "                        fallback_parent_id: jQuery(\"#";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 16, $this->getSourceContext()); })()), "parent", array()), "offsetGet", array(0 => (isset($context["fallback_parent_field"]) || array_key_exists("fallback_parent_field", $context) ? $context["fallback_parent_field"] : (function () { throw new Twig_Error_Runtime('Variable "fallback_parent_field" does not exist.', 16, $this->getSourceContext()); })())), "method"), "vars", array()), "id", array()), "html", null, true);
            echo "\").val(),
                        ";
        }
        // line 18
        echo "                        entity_alias: \"";
        echo twig_escape_filter($this->env, (isset($context["entity_alias"]) || array_key_exists("entity_alias", $context) ? $context["entity_alias"] : (function () { throw new Twig_Error_Runtime('Variable "entity_alias" does not exist.', 18, $this->getSourceContext()); })()), "html", null, true);
        echo "\",
                        empty_value: \"";
        // line 19
        echo twig_escape_filter($this->env, (isset($context["empty_value"]) || array_key_exists("empty_value", $context) ? $context["empty_value"] : (function () { throw new Twig_Error_Runtime('Variable "empty_value" does not exist.', 19, $this->getSourceContext()); })()), "html", null, true);
        echo "\",
                        excluded_entity_id: \"";
        // line 20
        echo twig_escape_filter($this->env, (isset($context["excluded_entity_id"]) || array_key_exists("excluded_entity_id", $context) ? $context["excluded_entity_id"] : (function () { throw new Twig_Error_Runtime('Variable "excluded_entity_id" does not exist.', 20, $this->getSourceContext()); })()), "html", null, true);
        echo "\",
                        choice_translation_domain: \"";
        // line 21
        echo twig_escape_filter($this->env, (isset($context["choice_translation_domain"]) || array_key_exists("choice_translation_domain", $context) ? $context["choice_translation_domain"] : (function () { throw new Twig_Error_Runtime('Variable "choice_translation_domain" does not exist.', 21, $this->getSourceContext()); })()), "html", null, true);
        echo "\",
                        choice_title_translation_part: \"";
        // line 22
        echo twig_escape_filter($this->env, (isset($context["choice_title_translation_part"]) || array_key_exists("choice_title_translation_part", $context) ? $context["choice_title_translation_part"] : (function () { throw new Twig_Error_Runtime('Variable "choice_title_translation_part" does not exist.', 22, $this->getSourceContext()); })()), "html", null, true);
        echo "\",
                        callback_parameters: '";
        // line 23
        echo json_encode((isset($context["callback_parameters"]) || array_key_exists("callback_parameters", $context) ? $context["callback_parameters"] : (function () { throw new Twig_Error_Runtime('Variable "callback_parameters" does not exist.', 23, $this->getSourceContext()); })()));
        echo "'
                    },
                    url:\"";
        // line 25
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("dependent_select");
        echo "\",
                    success: function(msg){
                        if (msg != ''){
                            jQuery(\"select#";
        // line 28
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 28, $this->getSourceContext()); })()), "vars", array()), "id", array()), "html", null, true);
        echo "\").get(0).disabled = false;
                            jQuery(\"select#";
        // line 29
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 29, $this->getSourceContext()); })()), "vars", array()), "id", array()), "html", null, true);
        echo "\").html(msg).show();
                            jQuery.each(jQuery(\"select#";
        // line 30
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 30, $this->getSourceContext()); })()), "vars", array()), "id", array()), "html", null, true);
        echo " option\"), function (index, option){
                                if (jQuery(option).val() == selected_index)
                                    jQuery(option).prop('selected', true);
                            });
                            jQuery(\"select#";
        // line 34
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 34, $this->getSourceContext()); })()), "vars", array()), "id", array()), "html", null, true);
        echo "\").trigger('change');
                        } else {
                            jQuery(\"select#";
        // line 36
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 36, $this->getSourceContext()); })()), "vars", array()), "id", array()), "html", null, true);
        echo "\").html('<em>";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans((isset($context["no_result_msg"]) || array_key_exists("no_result_msg", $context) ? $context["no_result_msg"] : (function () { throw new Twig_Error_Runtime('Variable "no_result_msg" does not exist.', 36, $this->getSourceContext()); })())), "html", null, true);
        echo "</em>');
                        }
                    },
                    error: function(xhr, ajaxOptions, thrownError){
                        jQuery('html').html(xhr.responseText);
                    }
                });
            });

            jQuery(\"select#";
        // line 45
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 45, $this->getSourceContext()); })()), "parent", array()), "offsetGet", array(0 => (isset($context["parent_field"]) || array_key_exists("parent_field", $context) ? $context["parent_field"] : (function () { throw new Twig_Error_Runtime('Variable "parent_field" does not exist.', 45, $this->getSourceContext()); })())), "method"), "vars", array()), "id", array()), "html", null, true);
        echo "\").trigger('change');
        });
    </script>

";
        
        $__internal_5809b77e6fbc31fbcd688402f51fbe91521641b916d20c072355b762e1e1628c->leave($__internal_5809b77e6fbc31fbcd688402f51fbe91521641b916d20c072355b762e1e1628c_prof);

        
        $__internal_32d5764d17c08d19812d8697f75d4b43c193e9f26c9ea6b513d06dffadca87ba->leave($__internal_32d5764d17c08d19812d8697f75d4b43c193e9f26c9ea6b513d06dffadca87ba_prof);

    }

    public function getTemplateName()
    {
        return "DependentSelectBundle::fields.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  145 => 45,  131 => 36,  126 => 34,  119 => 30,  115 => 29,  111 => 28,  105 => 25,  100 => 23,  96 => 22,  92 => 21,  88 => 20,  84 => 19,  79 => 18,  73 => 16,  71 => 15,  63 => 10,  59 => 9,  55 => 8,  47 => 3,  44 => 2,  26 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% block dependent_filtered_entity_widget %}

    <select {{ block('widget_attributes') }} class=\"form-control\" disabled></select>

    <script type=\"text/javascript\">
        jQuery(function(){

            jQuery(\"select#{{ form.parent.offsetGet( parent_field ).vars.id }}\").change( function() {
                var selected_index = {{ value ? value : 0 }};
                jQuery(\"select#{{ form.vars.id }}\").get(0).disabled = true;
                jQuery.ajax({
                    type: \"POST\",
                    data: {
                        parent_id: jQuery(this).val(),
                        {% if fallback_parent_field is not null %}
                        fallback_parent_id: jQuery(\"#{{ form.parent.offsetGet( fallback_parent_field ).vars.id }}\").val(),
                        {% endif %}
                        entity_alias: \"{{ entity_alias }}\",
                        empty_value: \"{{ empty_value }}\",
                        excluded_entity_id: \"{{ excluded_entity_id }}\",
                        choice_translation_domain: \"{{ choice_translation_domain }}\",
                        choice_title_translation_part: \"{{ choice_title_translation_part }}\",
                        callback_parameters: '{{ callback_parameters|json_encode|raw }}'
                    },
                    url:\"{{ path('dependent_select') }}\",
                    success: function(msg){
                        if (msg != ''){
                            jQuery(\"select#{{ form.vars.id }}\").get(0).disabled = false;
                            jQuery(\"select#{{ form.vars.id }}\").html(msg).show();
                            jQuery.each(jQuery(\"select#{{ form.vars.id }} option\"), function (index, option){
                                if (jQuery(option).val() == selected_index)
                                    jQuery(option).prop('selected', true);
                            });
                            jQuery(\"select#{{ form.vars.id }}\").trigger('change');
                        } else {
                            jQuery(\"select#{{ form.vars.id }}\").html('<em>{{ no_result_msg|trans() }}</em>');
                        }
                    },
                    error: function(xhr, ajaxOptions, thrownError){
                        jQuery('html').html(xhr.responseText);
                    }
                });
            });

            jQuery(\"select#{{ form.parent.offsetGet( parent_field ).vars.id }}\").trigger('change');
        });
    </script>

{% endblock %}
", "DependentSelectBundle::fields.html.twig", "/home/sj/PhpstormProjects/symfony4-project/vendor/kabuto/dependent-select-bundle/Resources/views/fields.html.twig");
    }
}
