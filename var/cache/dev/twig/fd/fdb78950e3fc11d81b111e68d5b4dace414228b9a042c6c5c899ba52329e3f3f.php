<?php

/* statistics.html.twig */
class __TwigTemplate_5bc5f0e69c37f922e23c61c8bdf9e0775fca32175f0141ccc6c6d8f86585a56b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "statistics.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_70cce6aad74460fa065443a909d7c3935ccbf1e985e1489952f9bb9a58dd91d0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_70cce6aad74460fa065443a909d7c3935ccbf1e985e1489952f9bb9a58dd91d0->enter($__internal_70cce6aad74460fa065443a909d7c3935ccbf1e985e1489952f9bb9a58dd91d0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "statistics.html.twig"));

        $__internal_9055f3993042a16b5074e503ec0abc5964ac1078dcc2894420fce386b012c8d0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9055f3993042a16b5074e503ec0abc5964ac1078dcc2894420fce386b012c8d0->enter($__internal_9055f3993042a16b5074e503ec0abc5964ac1078dcc2894420fce386b012c8d0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "statistics.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_70cce6aad74460fa065443a909d7c3935ccbf1e985e1489952f9bb9a58dd91d0->leave($__internal_70cce6aad74460fa065443a909d7c3935ccbf1e985e1489952f9bb9a58dd91d0_prof);

        
        $__internal_9055f3993042a16b5074e503ec0abc5964ac1078dcc2894420fce386b012c8d0->leave($__internal_9055f3993042a16b5074e503ec0abc5964ac1078dcc2894420fce386b012c8d0_prof);

    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        $__internal_79ef72c4d67011dfedde34118ca9288b930b8c6f9b6c94e8b90f8fa18e72ea90 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_79ef72c4d67011dfedde34118ca9288b930b8c6f9b6c94e8b90f8fa18e72ea90->enter($__internal_79ef72c4d67011dfedde34118ca9288b930b8c6f9b6c94e8b90f8fa18e72ea90_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        $__internal_cba6beb02e21c37a0b32d9e7c1382878e135e6ef76a139bc94a0276a68a39b80 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_cba6beb02e21c37a0b32d9e7c1382878e135e6ef76a139bc94a0276a68a39b80->enter($__internal_cba6beb02e21c37a0b32d9e7c1382878e135e6ef76a139bc94a0276a68a39b80_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 4
        echo "    <div class=\"container\">
        <h2>User registrations by city:</h2>
        <ul>
        ";
        // line 7
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["userRegistrations"]) || array_key_exists("userRegistrations", $context) ? $context["userRegistrations"] : (function () { throw new Twig_Error_Runtime('Variable "userRegistrations" does not exist.', 7, $this->getSourceContext()); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["city"]) {
            // line 8
            echo "            <li>";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["city"], "name", array()), "html", null, true);
            echo " - This month: ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["city"], "thisMonth", array()), "html", null, true);
            echo ". Total: ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["city"], "total", array()), "html", null, true);
            echo "</li>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['city'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 10
        echo "        </ul>
        <h2>User logins for last week:</h2>
        <ul>
        ";
        // line 13
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["visits"]) || array_key_exists("visits", $context) ? $context["visits"] : (function () { throw new Twig_Error_Runtime('Variable "visits" does not exist.', 13, $this->getSourceContext()); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["dayVisits"]) {
            // line 14
            echo "            <li>";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["dayVisits"], "c_date", array()), "html", null, true);
            echo " - ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["dayVisits"], "count", array()), "html", null, true);
            echo "</li>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['dayVisits'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 16
        echo "        </ul>
    </div>
";
        
        $__internal_cba6beb02e21c37a0b32d9e7c1382878e135e6ef76a139bc94a0276a68a39b80->leave($__internal_cba6beb02e21c37a0b32d9e7c1382878e135e6ef76a139bc94a0276a68a39b80_prof);

        
        $__internal_79ef72c4d67011dfedde34118ca9288b930b8c6f9b6c94e8b90f8fa18e72ea90->leave($__internal_79ef72c4d67011dfedde34118ca9288b930b8c6f9b6c94e8b90f8fa18e72ea90_prof);

    }

    public function getTemplateName()
    {
        return "statistics.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  91 => 16,  80 => 14,  76 => 13,  71 => 10,  58 => 8,  54 => 7,  49 => 4,  40 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block content %}
    <div class=\"container\">
        <h2>User registrations by city:</h2>
        <ul>
        {% for city in userRegistrations %}
            <li>{{ city.name }} - This month: {{ city.thisMonth}}. Total: {{ city.total }}</li>
        {% endfor %}
        </ul>
        <h2>User logins for last week:</h2>
        <ul>
        {% for dayVisits in visits %}
            <li>{{ dayVisits.c_date }} - {{ dayVisits.count }}</li>
        {% endfor %}
        </ul>
    </div>
{% endblock %}
", "statistics.html.twig", "/home/sj/PhpstormProjects/symfony4-project/templates/statistics.html.twig");
    }
}
