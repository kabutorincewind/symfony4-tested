<?php

/* @Twig/images/icon-minus-square-o.svg */
class __TwigTemplate_c7de59b86704c7dd1a1ff5da682dc72b29ab94041347b2d89e9ce8a819b47f71 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c05170dc74acae55b256368a4b0335d458885161650f2c9b2732b240712e3ec3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c05170dc74acae55b256368a4b0335d458885161650f2c9b2732b240712e3ec3->enter($__internal_c05170dc74acae55b256368a4b0335d458885161650f2c9b2732b240712e3ec3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/images/icon-minus-square-o.svg"));

        $__internal_0ef48ef61925cff0c6a8d775a2780d9f25854ea878a6774815dd69678fb5b5d8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0ef48ef61925cff0c6a8d775a2780d9f25854ea878a6774815dd69678fb5b5d8->enter($__internal_0ef48ef61925cff0c6a8d775a2780d9f25854ea878a6774815dd69678fb5b5d8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/images/icon-minus-square-o.svg"));

        // line 1
        echo "<svg width=\"1792\" height=\"1792\" viewBox=\"0 0 1792 1792\" xmlns=\"http://www.w3.org/2000/svg\"><path d=\"M1344 800v64q0 14-9 23t-23 9H480q-14 0-23-9t-9-23v-64q0-14 9-23t23-9h832q14 0 23 9t9 23zm128 448V416q0-66-47-113t-113-47H480q-66 0-113 47t-47 113v832q0 66 47 113t113 47h832q66 0 113-47t47-113zm128-832v832q0 119-84.5 203.5T1312 1536H480q-119 0-203.5-84.5T192 1248V416q0-119 84.5-203.5T480 128h832q119 0 203.5 84.5T1600 416z\"/></svg>
";
        
        $__internal_c05170dc74acae55b256368a4b0335d458885161650f2c9b2732b240712e3ec3->leave($__internal_c05170dc74acae55b256368a4b0335d458885161650f2c9b2732b240712e3ec3_prof);

        
        $__internal_0ef48ef61925cff0c6a8d775a2780d9f25854ea878a6774815dd69678fb5b5d8->leave($__internal_0ef48ef61925cff0c6a8d775a2780d9f25854ea878a6774815dd69678fb5b5d8_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/images/icon-minus-square-o.svg";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<svg width=\"1792\" height=\"1792\" viewBox=\"0 0 1792 1792\" xmlns=\"http://www.w3.org/2000/svg\"><path d=\"M1344 800v64q0 14-9 23t-23 9H480q-14 0-23-9t-9-23v-64q0-14 9-23t23-9h832q14 0 23 9t9 23zm128 448V416q0-66-47-113t-113-47H480q-66 0-113 47t-47 113v832q0 66 47 113t113 47h832q66 0 113-47t47-113zm128-832v832q0 119-84.5 203.5T1312 1536H480q-119 0-203.5-84.5T192 1248V416q0-119 84.5-203.5T480 128h832q119 0 203.5 84.5T1600 416z\"/></svg>
", "@Twig/images/icon-minus-square-o.svg", "/home/sj/PhpstormProjects/symfony4-project/vendor/symfony/twig-bundle/Resources/views/images/icon-minus-square-o.svg");
    }
}
