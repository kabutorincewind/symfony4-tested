<?php

/* @Twig/layout.html.twig */
class __TwigTemplate_4d1849720c8a833584675437ba5d371605ae24cc697a6f626f34c46a62bf0c01 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'head' => array($this, 'block_head'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2e5aacaaaf355e364d9fe962e5abc08d49247aed3077b5c3bb1407e23b64ed4f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2e5aacaaaf355e364d9fe962e5abc08d49247aed3077b5c3bb1407e23b64ed4f->enter($__internal_2e5aacaaaf355e364d9fe962e5abc08d49247aed3077b5c3bb1407e23b64ed4f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/layout.html.twig"));

        $__internal_8d6d6156c63294a8fc4114d5412a7375d3cf93b1163efed902388ce72f005b3b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8d6d6156c63294a8fc4114d5412a7375d3cf93b1163efed902388ce72f005b3b->enter($__internal_8d6d6156c63294a8fc4114d5412a7375d3cf93b1163efed902388ce72f005b3b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/layout.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getCharset(), "html", null, true);
        echo "\" />
        <meta name=\"robots\" content=\"noindex,nofollow\" />
        <meta name=\"viewport\" content=\"width=device-width,initial-scale=1\" />
        <title>";
        // line 7
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        <link rel=\"icon\" type=\"image/png\" href=\"";
        // line 8
        echo twig_include($this->env, $context, "@Twig/images/favicon.png.base64");
        echo "\">
        <style>";
        // line 9
        echo twig_include($this->env, $context, "@Twig/exception.css.twig");
        echo "</style>
        ";
        // line 10
        $this->displayBlock('head', $context, $blocks);
        // line 11
        echo "    </head>
    <body>
        <header>
            <div class=\"container\">
                <h1 class=\"logo\">";
        // line 15
        echo twig_include($this->env, $context, "@Twig/images/symfony-logo.svg");
        echo " Symfony Exception</h1>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/doc\">
                        <span class=\"icon\">";
        // line 19
        echo twig_include($this->env, $context, "@Twig/images/icon-book.svg");
        echo "</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Docs
                    </a>
                </div>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/support\">
                        <span class=\"icon\">";
        // line 26
        echo twig_include($this->env, $context, "@Twig/images/icon-support.svg");
        echo "</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Support
                    </a>
                </div>
            </div>
        </header>

        ";
        // line 33
        $this->displayBlock('body', $context, $blocks);
        // line 34
        echo "        ";
        echo twig_include($this->env, $context, "@Twig/base_js.html.twig");
        echo "
    </body>
</html>
";
        
        $__internal_2e5aacaaaf355e364d9fe962e5abc08d49247aed3077b5c3bb1407e23b64ed4f->leave($__internal_2e5aacaaaf355e364d9fe962e5abc08d49247aed3077b5c3bb1407e23b64ed4f_prof);

        
        $__internal_8d6d6156c63294a8fc4114d5412a7375d3cf93b1163efed902388ce72f005b3b->leave($__internal_8d6d6156c63294a8fc4114d5412a7375d3cf93b1163efed902388ce72f005b3b_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_a6a3938458eb9369f54918b0d4d6599aa57755a33e65a780b04777e16250dbd9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a6a3938458eb9369f54918b0d4d6599aa57755a33e65a780b04777e16250dbd9->enter($__internal_a6a3938458eb9369f54918b0d4d6599aa57755a33e65a780b04777e16250dbd9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_38a8fc24656f42b4cfd65f2136a524ba9958084653e874b11afa338ad393a1ba = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_38a8fc24656f42b4cfd65f2136a524ba9958084653e874b11afa338ad393a1ba->enter($__internal_38a8fc24656f42b4cfd65f2136a524ba9958084653e874b11afa338ad393a1ba_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        
        $__internal_38a8fc24656f42b4cfd65f2136a524ba9958084653e874b11afa338ad393a1ba->leave($__internal_38a8fc24656f42b4cfd65f2136a524ba9958084653e874b11afa338ad393a1ba_prof);

        
        $__internal_a6a3938458eb9369f54918b0d4d6599aa57755a33e65a780b04777e16250dbd9->leave($__internal_a6a3938458eb9369f54918b0d4d6599aa57755a33e65a780b04777e16250dbd9_prof);

    }

    // line 10
    public function block_head($context, array $blocks = array())
    {
        $__internal_e02e17ab5ae30b4948e0b8b7f98ac840472713ca2bedb15bcc1949ff5a273858 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e02e17ab5ae30b4948e0b8b7f98ac840472713ca2bedb15bcc1949ff5a273858->enter($__internal_e02e17ab5ae30b4948e0b8b7f98ac840472713ca2bedb15bcc1949ff5a273858_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_878095a206e94c5b2108a4812a550efc68f27e1e10443add30610c8554ea4452 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_878095a206e94c5b2108a4812a550efc68f27e1e10443add30610c8554ea4452->enter($__internal_878095a206e94c5b2108a4812a550efc68f27e1e10443add30610c8554ea4452_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        
        $__internal_878095a206e94c5b2108a4812a550efc68f27e1e10443add30610c8554ea4452->leave($__internal_878095a206e94c5b2108a4812a550efc68f27e1e10443add30610c8554ea4452_prof);

        
        $__internal_e02e17ab5ae30b4948e0b8b7f98ac840472713ca2bedb15bcc1949ff5a273858->leave($__internal_e02e17ab5ae30b4948e0b8b7f98ac840472713ca2bedb15bcc1949ff5a273858_prof);

    }

    // line 33
    public function block_body($context, array $blocks = array())
    {
        $__internal_c22f4de83b2bd09b1562ffda92564e78f6dff1ab44c1a8d9a6eaabb338c7defa = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c22f4de83b2bd09b1562ffda92564e78f6dff1ab44c1a8d9a6eaabb338c7defa->enter($__internal_c22f4de83b2bd09b1562ffda92564e78f6dff1ab44c1a8d9a6eaabb338c7defa_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_e5e5127d88a9925ffa96a6593d775558d6af00360ac007905256f6c92c2cc59f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e5e5127d88a9925ffa96a6593d775558d6af00360ac007905256f6c92c2cc59f->enter($__internal_e5e5127d88a9925ffa96a6593d775558d6af00360ac007905256f6c92c2cc59f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_e5e5127d88a9925ffa96a6593d775558d6af00360ac007905256f6c92c2cc59f->leave($__internal_e5e5127d88a9925ffa96a6593d775558d6af00360ac007905256f6c92c2cc59f_prof);

        
        $__internal_c22f4de83b2bd09b1562ffda92564e78f6dff1ab44c1a8d9a6eaabb338c7defa->leave($__internal_c22f4de83b2bd09b1562ffda92564e78f6dff1ab44c1a8d9a6eaabb338c7defa_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  137 => 33,  120 => 10,  103 => 7,  88 => 34,  86 => 33,  76 => 26,  66 => 19,  59 => 15,  53 => 11,  51 => 10,  47 => 9,  43 => 8,  39 => 7,  33 => 4,  28 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"{{ _charset }}\" />
        <meta name=\"robots\" content=\"noindex,nofollow\" />
        <meta name=\"viewport\" content=\"width=device-width,initial-scale=1\" />
        <title>{% block title %}{% endblock %}</title>
        <link rel=\"icon\" type=\"image/png\" href=\"{{ include('@Twig/images/favicon.png.base64') }}\">
        <style>{{ include('@Twig/exception.css.twig') }}</style>
        {% block head %}{% endblock %}
    </head>
    <body>
        <header>
            <div class=\"container\">
                <h1 class=\"logo\">{{ include('@Twig/images/symfony-logo.svg') }} Symfony Exception</h1>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/doc\">
                        <span class=\"icon\">{{ include('@Twig/images/icon-book.svg') }}</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Docs
                    </a>
                </div>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/support\">
                        <span class=\"icon\">{{ include('@Twig/images/icon-support.svg') }}</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Support
                    </a>
                </div>
            </div>
        </header>

        {% block body %}{% endblock %}
        {{ include('@Twig/base_js.html.twig') }}
    </body>
</html>
", "@Twig/layout.html.twig", "/home/sj/PhpstormProjects/symfony4-project/vendor/symfony/twig-bundle/Resources/views/layout.html.twig");
    }
}
