<?php

/* base.html.twig */
class __TwigTemplate_836fc69dec5d69f4f968d286114596b52d3eabfd9c16e3cdea72f9b0ef39c4cb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'scripts' => array($this, 'block_scripts'),
            'nav' => array($this, 'block_nav'),
            'flashmessages' => array($this, 'block_flashmessages'),
            'content' => array($this, 'block_content'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f4d75b6072952c9e4005dd19e9d3824f0e8341b68585367b54380f1c902de13c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f4d75b6072952c9e4005dd19e9d3824f0e8341b68585367b54380f1c902de13c->enter($__internal_f4d75b6072952c9e4005dd19e9d3824f0e8341b68585367b54380f1c902de13c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        $__internal_3842991cc1a88da5915b0ec1eb5694c185ee78eb38ac776e17f14e2e36ee6edc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3842991cc1a88da5915b0ec1eb5694c185ee78eb38ac776e17f14e2e36ee6edc->enter($__internal_3842991cc1a88da5915b0ec1eb5694c185ee78eb38ac776e17f14e2e36ee6edc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
<head>
    <meta charset=\"UTF-8\">
    <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>

    ";
        // line 7
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 10
        echo "    ";
        $this->displayBlock('scripts', $context, $blocks);
        // line 13
        echo "
    <script src=\"//cdnjs.cloudflare.com/ajax/libs/lodash.js/4.10.0/lodash.js\"></script>

    <script type=\"text/lodash-template\" id=\"ltpl-movie-thumb\">
        <div class=\"nav-item menu-item movie-block\" data-movie-id=\"<{movie_id}>\">
            <img src=\"<{originalCover}>\" alt=\"\" class=\"img-responsive img-thumbnail\">
            <div>
                <h1><{name}></h1>
                <% if(typeof rating_imdb_value != 'undefined' && rating_imdb_value != '0') { %>
                    <span class=\"pull-left\">IMDB: <{rating_imdb_value}></span>
                <% } %>
                <span class=\"pull-right\"><{hit}> <span class=\"glyphicon glyphicon-heart\"></span></span>
                <div class=\"clearfix\"></div>
            </div>
        </div>
    </script>

    <script type=\"text/lodash-template\" id=\"ltpl-movie-single\">
        <div class=\"main\">
            <div class=\"title\">
                <span class=\"glyphicon glyphicon-arrow-left nav-item return-to-list\"></span>&nbsp;&nbsp;&nbsp;
                <h1><{name}></h1>
            </div>
            <img class=\"poster\" src=\"<{covers[0].original}>\"/>
            <div class=\"description\">
                Год: <{year}><br/>
                Страна: <{countries.join(', ')}><br/>
                Режиссер: <{directors.join(', ')}><br/><br/>
                <{description}>
            </div>
            <div class=\"clearfix\">&nbsp;</div>
        </div>
        <div class=\"navs\">
            <div class=\"ratings\">
                <% if(typeof rating_imdb_value != 'undefined' && rating_imdb_value != '0') { %>
                <span>IMDB: <{rating_imdb_value}></span>
                <% } %>
                <span><span class=\"glyphicon glyphicon-heart\"></span> <{hit}></span>
            </div>
            <div class=\"menu\">
                <div class=\"play-block nav-item\" data-stream-link=\"<{stream}>\">
                    <span class=\"glyphicon glyphicon-play\"></span><br/>
                    Смотреть
                </div>
                <div class=\"clearfix\">&nbsp;</div>
            </div>
        </div>
    </script>

    <script>
        _.templateSettings.interpolate = /\\<\\{([\\s\\S]+?)\\}\\>/g
        _.templateSettings.evaluate = /\\<%([\\s\\S]+?)%\\>/g

        html = '';

//        for (var i = 0; i < movies.length; i++) {
//            movies[i].originalCover = DataProvider.remoteHost + movies[i].originalCover;
//            html = html + (_.template(\$('script#ltpl-movie-thumb').text()))(movies[i]);
//        }
//
//        \$('.movies-stack').html(html);
//
//        movieInfoHtml = (_.template(\$(\"script#ltpl-movie-single\").text()))(movie);
//                \$('.movie-info').html(movieInfoHtml);
    </script>

    <script src=\"https://api-maps.yandex.ru/2.1/?lang=ru_RU\" type=\"text/javascript\">
    </script>
    <script type=\"text/javascript\">
        ymaps.ready(init);
        var myMap,
            myPlacemark;

        function init(){
            myMap = new ymaps.Map(\"map\", {
                center: [55.76, 37.64],
                zoom: 7
            });

            myPlacemark = new ymaps.Placemark([55.76, 37.64], {
                hintContent: 'Москва!',
                balloonContent: 'Столица России'
            });

            myMap.geoObjects.add(myPlacemark);
        }
        document.ready = function(){
            document.querySelector('#edit_city').onblur = function(evy) {
                for (var i = 0; i < evy.target.options.length; i++) {
                    ids.push(evy.target.options[i].value)
                }

                \$.ajax({
                    type: \"POST\",
                    data: {'ids': ids},
                    url: \"/getCoordinates\",
                    success: function (data) {
                        console.log(data);
                        for (var n = 0; n < data.length; n++) {
                            elPlacemark = new ymaps.Placemark([data[n].latitude, data[n].longitude], {
                                hintContent: el.name,
                                balloonContent: '--\\\\||--'
                            });
                            myMap.geoObjects.add(elPlacemark);
                        }
                    }
                });
            }
        };


    </script>


</head>

<body>
<nav class=\"navbar navbar-expand-md navbar-light bg-light fixed-top\">
    <a class=\"navbar-brand\" href=\"";
        // line 131
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("homepage");
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("site.name"), "html", null, true);
        echo "</a>
    <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarCollapse\"
            aria-controls=\"navbarCollapse\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">
        <span class=\"navbar-toggler-icon\"></span>
    </button>

    <div class=\"collapse navbar-collapse\" id=\"navbarCollapse\">
        ";
        // line 138
        $this->displayBlock('nav', $context, $blocks);
        // line 157
        echo "    </div>
</nav>

<div id=\"map\" style=\"width: 640px; height: 420px\">&mdash;</div>

<div class=\"main-content\" style=\"margin-top: 56px;\">
    ";
        // line 163
        $this->displayBlock('flashmessages', $context, $blocks);
        // line 176
        echo "    <div style=\"min-height: 56px\"></div>
    ";
        // line 177
        $this->displayBlock('content', $context, $blocks);
        // line 178
        echo "

</div>
";
        // line 181
        $this->displayBlock('javascripts', $context, $blocks);
        // line 184
        echo "</body>

</html>
";
        
        $__internal_f4d75b6072952c9e4005dd19e9d3824f0e8341b68585367b54380f1c902de13c->leave($__internal_f4d75b6072952c9e4005dd19e9d3824f0e8341b68585367b54380f1c902de13c_prof);

        
        $__internal_3842991cc1a88da5915b0ec1eb5694c185ee78eb38ac776e17f14e2e36ee6edc->leave($__internal_3842991cc1a88da5915b0ec1eb5694c185ee78eb38ac776e17f14e2e36ee6edc_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_f217438123bc5286178f9d58b409f6286ebf5d86dee7c908daa8b9c7f1513b73 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f217438123bc5286178f9d58b409f6286ebf5d86dee7c908daa8b9c7f1513b73->enter($__internal_f217438123bc5286178f9d58b409f6286ebf5d86dee7c908daa8b9c7f1513b73_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_e6b2898c9ef15b6d815a522acf50478e443eb0d1d2f1931db8a8623e03ae9a30 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e6b2898c9ef15b6d815a522acf50478e443eb0d1d2f1931db8a8623e03ae9a30->enter($__internal_e6b2898c9ef15b6d815a522acf50478e443eb0d1d2f1931db8a8623e03ae9a30_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("site.name"), "html", null, true);
        
        $__internal_e6b2898c9ef15b6d815a522acf50478e443eb0d1d2f1931db8a8623e03ae9a30->leave($__internal_e6b2898c9ef15b6d815a522acf50478e443eb0d1d2f1931db8a8623e03ae9a30_prof);

        
        $__internal_f217438123bc5286178f9d58b409f6286ebf5d86dee7c908daa8b9c7f1513b73->leave($__internal_f217438123bc5286178f9d58b409f6286ebf5d86dee7c908daa8b9c7f1513b73_prof);

    }

    // line 7
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_6d656ea0852847d22c4c7bababdd66b9164a29bdeb6e7eed39935545722301a3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6d656ea0852847d22c4c7bababdd66b9164a29bdeb6e7eed39935545722301a3->enter($__internal_6d656ea0852847d22c4c7bababdd66b9164a29bdeb6e7eed39935545722301a3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_19634826d395818449d476598eb74364684991dc57f12f80d9d558162cc48300 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_19634826d395818449d476598eb74364684991dc57f12f80d9d558162cc48300->enter($__internal_19634826d395818449d476598eb74364684991dc57f12f80d9d558162cc48300_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 8
        echo "        <link rel=\"stylesheet\" href=\"/build/app.css\">
    ";
        
        $__internal_19634826d395818449d476598eb74364684991dc57f12f80d9d558162cc48300->leave($__internal_19634826d395818449d476598eb74364684991dc57f12f80d9d558162cc48300_prof);

        
        $__internal_6d656ea0852847d22c4c7bababdd66b9164a29bdeb6e7eed39935545722301a3->leave($__internal_6d656ea0852847d22c4c7bababdd66b9164a29bdeb6e7eed39935545722301a3_prof);

    }

    // line 10
    public function block_scripts($context, array $blocks = array())
    {
        $__internal_b0216ebc5bdbe102b27f512d50eeeb490950b26e67ba86d8d45f8ce9d4245178 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b0216ebc5bdbe102b27f512d50eeeb490950b26e67ba86d8d45f8ce9d4245178->enter($__internal_b0216ebc5bdbe102b27f512d50eeeb490950b26e67ba86d8d45f8ce9d4245178_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "scripts"));

        $__internal_56f47328cbf015da7959c4e7073081f537c9064c76aa16ebc830e2775a0e5324 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_56f47328cbf015da7959c4e7073081f537c9064c76aa16ebc830e2775a0e5324->enter($__internal_56f47328cbf015da7959c4e7073081f537c9064c76aa16ebc830e2775a0e5324_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "scripts"));

        // line 11
        echo "        <script src=\"/build/app.js\" type=\"text/javascript\"></script>
    ";
        
        $__internal_56f47328cbf015da7959c4e7073081f537c9064c76aa16ebc830e2775a0e5324->leave($__internal_56f47328cbf015da7959c4e7073081f537c9064c76aa16ebc830e2775a0e5324_prof);

        
        $__internal_b0216ebc5bdbe102b27f512d50eeeb490950b26e67ba86d8d45f8ce9d4245178->leave($__internal_b0216ebc5bdbe102b27f512d50eeeb490950b26e67ba86d8d45f8ce9d4245178_prof);

    }

    // line 138
    public function block_nav($context, array $blocks = array())
    {
        $__internal_e17836b84ffeee9fdc06a36d3e7015cb640e439d94e85f64714b92ff21aad938 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e17836b84ffeee9fdc06a36d3e7015cb640e439d94e85f64714b92ff21aad938->enter($__internal_e17836b84ffeee9fdc06a36d3e7015cb640e439d94e85f64714b92ff21aad938_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "nav"));

        $__internal_5e7b1502d55ff3255b45b2c1ca0b2bdbedea31a48fc35a1b5ebe994eb1e39450 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5e7b1502d55ff3255b45b2c1ca0b2bdbedea31a48fc35a1b5ebe994eb1e39450->enter($__internal_5e7b1502d55ff3255b45b2c1ca0b2bdbedea31a48fc35a1b5ebe994eb1e39450_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "nav"));

        // line 139
        echo "            <ul class=\"navbar-nav\">

            </ul>
            <ul class=\"navbar-nav ml-auto\">
                <li class=\"nav-item\"><a class=\"nav-link\"
                                        href=\"";
        // line 144
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("statistics");
        echo "\">Statistics</a></li>
                ";
        // line 145
        if ($this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted("ROLE_USER")) {
            // line 146
            echo "                    <li class=\"nav-item\"><a class=\"nav-link\" href=\"";
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("user_edit");
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 146, $this->getSourceContext()); })()), "user", array()), "email", array()), "html", null, true);
            echo "</a>
                    </li>
                    <li class=\"nav-item\"><a class=\"nav-link\"
                                            href=\"";
            // line 149
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("security_logout");
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("user.logout"), "html", null, true);
            echo "</a></li>
                ";
        } else {
            // line 151
            echo "                    ";
            // line 152
            echo "                    <li class=\"nav-item\"><a class=\"nav-link\"
                                            href=\"";
            // line 153
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("security_login");
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("user.login"), "html", null, true);
            echo "</a></li>
                ";
        }
        // line 155
        echo "            </ul>
        ";
        
        $__internal_5e7b1502d55ff3255b45b2c1ca0b2bdbedea31a48fc35a1b5ebe994eb1e39450->leave($__internal_5e7b1502d55ff3255b45b2c1ca0b2bdbedea31a48fc35a1b5ebe994eb1e39450_prof);

        
        $__internal_e17836b84ffeee9fdc06a36d3e7015cb640e439d94e85f64714b92ff21aad938->leave($__internal_e17836b84ffeee9fdc06a36d3e7015cb640e439d94e85f64714b92ff21aad938_prof);

    }

    // line 163
    public function block_flashmessages($context, array $blocks = array())
    {
        $__internal_e1eeaad208384de049cb705e24cd3a6a411ee80ed89fe538437e22ef2ab4ccdd = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e1eeaad208384de049cb705e24cd3a6a411ee80ed89fe538437e22ef2ab4ccdd->enter($__internal_e1eeaad208384de049cb705e24cd3a6a411ee80ed89fe538437e22ef2ab4ccdd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "flashmessages"));

        $__internal_714af76ddf572dc6311417878a458cf770f5fe8cf522793037d4c6eb36208eb2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_714af76ddf572dc6311417878a458cf770f5fe8cf522793037d4c6eb36208eb2->enter($__internal_714af76ddf572dc6311417878a458cf770f5fe8cf522793037d4c6eb36208eb2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "flashmessages"));

        // line 164
        echo "        ";
        if ((twig_length_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 164, $this->getSourceContext()); })()), "session", array()), "flashbag", array()), "peekAll", array())) > 0)) {
            // line 165
            echo "            ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 165, $this->getSourceContext()); })()), "session", array()), "flashbag", array()), "all", array()));
            foreach ($context['_seq'] as $context["type"] => $context["messages"]) {
                // line 166
                echo "                ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($context["messages"]);
                foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
                    // line 167
                    echo "                    <div class=\"alert alert-dismissible alert-";
                    echo twig_escape_filter($this->env, (($context["type"]) ? ($context["type"]) : ("")), "html", null, true);
                    echo "\">
                        <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span
                                    aria-hidden=\"true\">&times;</span></button>
                        ";
                    // line 170
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans($context["message"], array(), ((array_key_exists("domain", $context)) ? (_twig_default_filter((isset($context["domain"]) || array_key_exists("domain", $context) ? $context["domain"] : (function () { throw new Twig_Error_Runtime('Variable "domain" does not exist.', 170, $this->getSourceContext()); })()), "messages")) : ("messages"))), "html", null, true);
                    echo "
                    </div>
                ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 173
                echo "            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['type'], $context['messages'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 174
            echo "        ";
        }
        // line 175
        echo "    ";
        
        $__internal_714af76ddf572dc6311417878a458cf770f5fe8cf522793037d4c6eb36208eb2->leave($__internal_714af76ddf572dc6311417878a458cf770f5fe8cf522793037d4c6eb36208eb2_prof);

        
        $__internal_e1eeaad208384de049cb705e24cd3a6a411ee80ed89fe538437e22ef2ab4ccdd->leave($__internal_e1eeaad208384de049cb705e24cd3a6a411ee80ed89fe538437e22ef2ab4ccdd_prof);

    }

    // line 177
    public function block_content($context, array $blocks = array())
    {
        $__internal_a86d31d6cd81f0b86a9a0923cabf8db48019fac81dd474fffcd63221f9ee2b36 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a86d31d6cd81f0b86a9a0923cabf8db48019fac81dd474fffcd63221f9ee2b36->enter($__internal_a86d31d6cd81f0b86a9a0923cabf8db48019fac81dd474fffcd63221f9ee2b36_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        $__internal_d8123f6ea2dddab2e4c373043966e37caf557c0ec44385e6f584a354dc88f55f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d8123f6ea2dddab2e4c373043966e37caf557c0ec44385e6f584a354dc88f55f->enter($__internal_d8123f6ea2dddab2e4c373043966e37caf557c0ec44385e6f584a354dc88f55f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        
        $__internal_d8123f6ea2dddab2e4c373043966e37caf557c0ec44385e6f584a354dc88f55f->leave($__internal_d8123f6ea2dddab2e4c373043966e37caf557c0ec44385e6f584a354dc88f55f_prof);

        
        $__internal_a86d31d6cd81f0b86a9a0923cabf8db48019fac81dd474fffcd63221f9ee2b36->leave($__internal_a86d31d6cd81f0b86a9a0923cabf8db48019fac81dd474fffcd63221f9ee2b36_prof);

    }

    // line 181
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_e4c81ada46f6685bfbbe28537285f3801520aaa4e09d870077cd5bb3c2f26b8b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e4c81ada46f6685bfbbe28537285f3801520aaa4e09d870077cd5bb3c2f26b8b->enter($__internal_e4c81ada46f6685bfbbe28537285f3801520aaa4e09d870077cd5bb3c2f26b8b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_72cb008d5f250ef318a0f2085ba334c8b21a0b47da0ea86ae0dfa01b72226a91 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_72cb008d5f250ef318a0f2085ba334c8b21a0b47da0ea86ae0dfa01b72226a91->enter($__internal_72cb008d5f250ef318a0f2085ba334c8b21a0b47da0ea86ae0dfa01b72226a91_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 182
        echo "    <script src=\"/build/app.js\" type=\"text/javascript\"></script>
";
        
        $__internal_72cb008d5f250ef318a0f2085ba334c8b21a0b47da0ea86ae0dfa01b72226a91->leave($__internal_72cb008d5f250ef318a0f2085ba334c8b21a0b47da0ea86ae0dfa01b72226a91_prof);

        
        $__internal_e4c81ada46f6685bfbbe28537285f3801520aaa4e09d870077cd5bb3c2f26b8b->leave($__internal_e4c81ada46f6685bfbbe28537285f3801520aaa4e09d870077cd5bb3c2f26b8b_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  419 => 182,  410 => 181,  393 => 177,  383 => 175,  380 => 174,  374 => 173,  365 => 170,  358 => 167,  353 => 166,  348 => 165,  345 => 164,  336 => 163,  325 => 155,  318 => 153,  315 => 152,  313 => 151,  306 => 149,  297 => 146,  295 => 145,  291 => 144,  284 => 139,  275 => 138,  264 => 11,  255 => 10,  244 => 8,  235 => 7,  217 => 5,  204 => 184,  202 => 181,  197 => 178,  195 => 177,  192 => 176,  190 => 163,  182 => 157,  180 => 138,  168 => 131,  48 => 13,  45 => 10,  43 => 7,  38 => 5,  32 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
<head>
    <meta charset=\"UTF-8\">
    <title>{% block title %}{{ 'site.name'|trans }}{% endblock %}</title>

    {% block stylesheets %}
        <link rel=\"stylesheet\" href=\"/build/app.css\">
    {% endblock %}
    {% block scripts %}
        <script src=\"/build/app.js\" type=\"text/javascript\"></script>
    {% endblock %}

    <script src=\"//cdnjs.cloudflare.com/ajax/libs/lodash.js/4.10.0/lodash.js\"></script>

    <script type=\"text/lodash-template\" id=\"ltpl-movie-thumb\">
        <div class=\"nav-item menu-item movie-block\" data-movie-id=\"<{movie_id}>\">
            <img src=\"<{originalCover}>\" alt=\"\" class=\"img-responsive img-thumbnail\">
            <div>
                <h1><{name}></h1>
                <% if(typeof rating_imdb_value != 'undefined' && rating_imdb_value != '0') { %>
                    <span class=\"pull-left\">IMDB: <{rating_imdb_value}></span>
                <% } %>
                <span class=\"pull-right\"><{hit}> <span class=\"glyphicon glyphicon-heart\"></span></span>
                <div class=\"clearfix\"></div>
            </div>
        </div>
    </script>

    <script type=\"text/lodash-template\" id=\"ltpl-movie-single\">
        <div class=\"main\">
            <div class=\"title\">
                <span class=\"glyphicon glyphicon-arrow-left nav-item return-to-list\"></span>&nbsp;&nbsp;&nbsp;
                <h1><{name}></h1>
            </div>
            <img class=\"poster\" src=\"<{covers[0].original}>\"/>
            <div class=\"description\">
                Год: <{year}><br/>
                Страна: <{countries.join(', ')}><br/>
                Режиссер: <{directors.join(', ')}><br/><br/>
                <{description}>
            </div>
            <div class=\"clearfix\">&nbsp;</div>
        </div>
        <div class=\"navs\">
            <div class=\"ratings\">
                <% if(typeof rating_imdb_value != 'undefined' && rating_imdb_value != '0') { %>
                <span>IMDB: <{rating_imdb_value}></span>
                <% } %>
                <span><span class=\"glyphicon glyphicon-heart\"></span> <{hit}></span>
            </div>
            <div class=\"menu\">
                <div class=\"play-block nav-item\" data-stream-link=\"<{stream}>\">
                    <span class=\"glyphicon glyphicon-play\"></span><br/>
                    Смотреть
                </div>
                <div class=\"clearfix\">&nbsp;</div>
            </div>
        </div>
    </script>

    <script>
        _.templateSettings.interpolate = /\\<\\{([\\s\\S]+?)\\}\\>/g
        _.templateSettings.evaluate = /\\<%([\\s\\S]+?)%\\>/g

        html = '';

//        for (var i = 0; i < movies.length; i++) {
//            movies[i].originalCover = DataProvider.remoteHost + movies[i].originalCover;
//            html = html + (_.template(\$('script#ltpl-movie-thumb').text()))(movies[i]);
//        }
//
//        \$('.movies-stack').html(html);
//
//        movieInfoHtml = (_.template(\$(\"script#ltpl-movie-single\").text()))(movie);
//                \$('.movie-info').html(movieInfoHtml);
    </script>

    <script src=\"https://api-maps.yandex.ru/2.1/?lang=ru_RU\" type=\"text/javascript\">
    </script>
    <script type=\"text/javascript\">
        ymaps.ready(init);
        var myMap,
            myPlacemark;

        function init(){
            myMap = new ymaps.Map(\"map\", {
                center: [55.76, 37.64],
                zoom: 7
            });

            myPlacemark = new ymaps.Placemark([55.76, 37.64], {
                hintContent: 'Москва!',
                balloonContent: 'Столица России'
            });

            myMap.geoObjects.add(myPlacemark);
        }
        document.ready = function(){
            document.querySelector('#edit_city').onblur = function(evy) {
                for (var i = 0; i < evy.target.options.length; i++) {
                    ids.push(evy.target.options[i].value)
                }

                \$.ajax({
                    type: \"POST\",
                    data: {'ids': ids},
                    url: \"/getCoordinates\",
                    success: function (data) {
                        console.log(data);
                        for (var n = 0; n < data.length; n++) {
                            elPlacemark = new ymaps.Placemark([data[n].latitude, data[n].longitude], {
                                hintContent: el.name,
                                balloonContent: '--\\\\||--'
                            });
                            myMap.geoObjects.add(elPlacemark);
                        }
                    }
                });
            }
        };


    </script>


</head>

<body>
<nav class=\"navbar navbar-expand-md navbar-light bg-light fixed-top\">
    <a class=\"navbar-brand\" href=\"{{ path('homepage') }}\">{{ 'site.name'|trans }}</a>
    <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarCollapse\"
            aria-controls=\"navbarCollapse\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">
        <span class=\"navbar-toggler-icon\"></span>
    </button>

    <div class=\"collapse navbar-collapse\" id=\"navbarCollapse\">
        {% block nav %}
            <ul class=\"navbar-nav\">

            </ul>
            <ul class=\"navbar-nav ml-auto\">
                <li class=\"nav-item\"><a class=\"nav-link\"
                                        href=\"{{ path('statistics') }}\">Statistics</a></li>
                {% if is_granted('ROLE_USER') %}
                    <li class=\"nav-item\"><a class=\"nav-link\" href=\"{{ path('user_edit') }}\">{{ app.user.email }}</a>
                    </li>
                    <li class=\"nav-item\"><a class=\"nav-link\"
                                            href=\"{{ path('security_logout') }}\">{{ 'user.logout'|trans }}</a></li>
                {% else %}
                    {#<li><a href=\"{{ path('user_register') }}\">{{ 'user.sign-up'|trans }}</a></li>#}
                    <li class=\"nav-item\"><a class=\"nav-link\"
                                            href=\"{{ path('security_login') }}\">{{ 'user.login'|trans }}</a></li>
                {% endif %}
            </ul>
        {% endblock %}
    </div>
</nav>

<div id=\"map\" style=\"width: 640px; height: 420px\">&mdash;</div>

<div class=\"main-content\" style=\"margin-top: 56px;\">
    {% block flashmessages %}
        {% if app.session.flashbag.peekAll|length > 0 %}
            {% for type, messages in app.session.flashbag.all %}
                {% for message in messages %}
                    <div class=\"alert alert-dismissible alert-{{ type ? type : '' }}\">
                        <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span
                                    aria-hidden=\"true\">&times;</span></button>
                        {{ message|trans({}, domain|default('messages')) }}
                    </div>
                {% endfor %}
            {% endfor %}
        {% endif %}
    {% endblock %}
    <div style=\"min-height: 56px\"></div>
    {% block content %}{% endblock %}


</div>
{% block javascripts %}
    <script src=\"/build/app.js\" type=\"text/javascript\"></script>
{% endblock %}
</body>

</html>
", "base.html.twig", "/home/sj/PhpstormProjects/symfony4-project/templates/base.html.twig");
    }
}
