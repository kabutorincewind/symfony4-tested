<?php

/* @WebProfiler/Collector/exception.html.twig */
class __TwigTemplate_10916ebf3c24e7cb717135eee0b138cc4c59245cc07016ef61e42ad17482db2d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/exception.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_631a6e7b9f3a6b79e7f1dddbed1dbdbf911e736801aecf0a9533abfd4ef1a030 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_631a6e7b9f3a6b79e7f1dddbed1dbdbf911e736801aecf0a9533abfd4ef1a030->enter($__internal_631a6e7b9f3a6b79e7f1dddbed1dbdbf911e736801aecf0a9533abfd4ef1a030_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/exception.html.twig"));

        $__internal_8ee290e951eb63115852be7eacc292efa7ceff67db9d49359d4664c44521a4eb = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8ee290e951eb63115852be7eacc292efa7ceff67db9d49359d4664c44521a4eb->enter($__internal_8ee290e951eb63115852be7eacc292efa7ceff67db9d49359d4664c44521a4eb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/exception.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_631a6e7b9f3a6b79e7f1dddbed1dbdbf911e736801aecf0a9533abfd4ef1a030->leave($__internal_631a6e7b9f3a6b79e7f1dddbed1dbdbf911e736801aecf0a9533abfd4ef1a030_prof);

        
        $__internal_8ee290e951eb63115852be7eacc292efa7ceff67db9d49359d4664c44521a4eb->leave($__internal_8ee290e951eb63115852be7eacc292efa7ceff67db9d49359d4664c44521a4eb_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_3614216a0c6294326a5ae28a736b44add7210b23ad196e52e95f8f5165105319 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3614216a0c6294326a5ae28a736b44add7210b23ad196e52e95f8f5165105319->enter($__internal_3614216a0c6294326a5ae28a736b44add7210b23ad196e52e95f8f5165105319_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_7a4b83fcf096f7fb3534ff9222b1ae46534abd7af198a36024f87aef23495b6c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7a4b83fcf096f7fb3534ff9222b1ae46534abd7af198a36024f87aef23495b6c->enter($__internal_7a4b83fcf096f7fb3534ff9222b1ae46534abd7af198a36024f87aef23495b6c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    ";
        if (twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || array_key_exists("collector", $context) ? $context["collector"] : (function () { throw new Twig_Error_Runtime('Variable "collector" does not exist.', 4, $this->getSourceContext()); })()), "hasexception", array())) {
            // line 5
            echo "        <style>
            ";
            // line 6
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception_css", array("token" => (isset($context["token"]) || array_key_exists("token", $context) ? $context["token"] : (function () { throw new Twig_Error_Runtime('Variable "token" does not exist.', 6, $this->getSourceContext()); })()))));
            echo "
        </style>
    ";
        }
        // line 9
        echo "    ";
        $this->displayParentBlock("head", $context, $blocks);
        echo "
";
        
        $__internal_7a4b83fcf096f7fb3534ff9222b1ae46534abd7af198a36024f87aef23495b6c->leave($__internal_7a4b83fcf096f7fb3534ff9222b1ae46534abd7af198a36024f87aef23495b6c_prof);

        
        $__internal_3614216a0c6294326a5ae28a736b44add7210b23ad196e52e95f8f5165105319->leave($__internal_3614216a0c6294326a5ae28a736b44add7210b23ad196e52e95f8f5165105319_prof);

    }

    // line 12
    public function block_menu($context, array $blocks = array())
    {
        $__internal_b7f289f64a8d3b9c5fa2384df8881f77c7ae2a1edf9b348c98e1583026e321da = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b7f289f64a8d3b9c5fa2384df8881f77c7ae2a1edf9b348c98e1583026e321da->enter($__internal_b7f289f64a8d3b9c5fa2384df8881f77c7ae2a1edf9b348c98e1583026e321da_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_4e90aa761785e341c098c4c755833adabf183f5fd7effb7e005c5e19a3a2409c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4e90aa761785e341c098c4c755833adabf183f5fd7effb7e005c5e19a3a2409c->enter($__internal_4e90aa761785e341c098c4c755833adabf183f5fd7effb7e005c5e19a3a2409c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 13
        echo "    <span class=\"label ";
        echo ((twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || array_key_exists("collector", $context) ? $context["collector"] : (function () { throw new Twig_Error_Runtime('Variable "collector" does not exist.', 13, $this->getSourceContext()); })()), "hasexception", array())) ? ("label-status-error") : ("disabled"));
        echo "\">
        <span class=\"icon\">";
        // line 14
        echo twig_include($this->env, $context, "@WebProfiler/Icon/exception.svg");
        echo "</span>
        <strong>Exception</strong>
        ";
        // line 16
        if (twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || array_key_exists("collector", $context) ? $context["collector"] : (function () { throw new Twig_Error_Runtime('Variable "collector" does not exist.', 16, $this->getSourceContext()); })()), "hasexception", array())) {
            // line 17
            echo "            <span class=\"count\">
                <span>1</span>
            </span>
        ";
        }
        // line 21
        echo "    </span>
";
        
        $__internal_4e90aa761785e341c098c4c755833adabf183f5fd7effb7e005c5e19a3a2409c->leave($__internal_4e90aa761785e341c098c4c755833adabf183f5fd7effb7e005c5e19a3a2409c_prof);

        
        $__internal_b7f289f64a8d3b9c5fa2384df8881f77c7ae2a1edf9b348c98e1583026e321da->leave($__internal_b7f289f64a8d3b9c5fa2384df8881f77c7ae2a1edf9b348c98e1583026e321da_prof);

    }

    // line 24
    public function block_panel($context, array $blocks = array())
    {
        $__internal_7583b3d3b0ffa05de9a5d122f6e170ed24a41489fed0e653b24d63b73a822cd4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7583b3d3b0ffa05de9a5d122f6e170ed24a41489fed0e653b24d63b73a822cd4->enter($__internal_7583b3d3b0ffa05de9a5d122f6e170ed24a41489fed0e653b24d63b73a822cd4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_ab82e870632c1074360bf671b05ae1dd7f1f6714c16ec1fe4a14123c802ae66d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ab82e870632c1074360bf671b05ae1dd7f1f6714c16ec1fe4a14123c802ae66d->enter($__internal_ab82e870632c1074360bf671b05ae1dd7f1f6714c16ec1fe4a14123c802ae66d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 25
        echo "    <h2>Exceptions</h2>

    ";
        // line 27
        if ( !twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || array_key_exists("collector", $context) ? $context["collector"] : (function () { throw new Twig_Error_Runtime('Variable "collector" does not exist.', 27, $this->getSourceContext()); })()), "hasexception", array())) {
            // line 28
            echo "        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    ";
        } else {
            // line 32
            echo "        <div class=\"sf-reset\">
            ";
            // line 33
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception", array("token" => (isset($context["token"]) || array_key_exists("token", $context) ? $context["token"] : (function () { throw new Twig_Error_Runtime('Variable "token" does not exist.', 33, $this->getSourceContext()); })()))));
            echo "
        </div>
    ";
        }
        
        $__internal_ab82e870632c1074360bf671b05ae1dd7f1f6714c16ec1fe4a14123c802ae66d->leave($__internal_ab82e870632c1074360bf671b05ae1dd7f1f6714c16ec1fe4a14123c802ae66d_prof);

        
        $__internal_7583b3d3b0ffa05de9a5d122f6e170ed24a41489fed0e653b24d63b73a822cd4->leave($__internal_7583b3d3b0ffa05de9a5d122f6e170ed24a41489fed0e653b24d63b73a822cd4_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/exception.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  138 => 33,  135 => 32,  129 => 28,  127 => 27,  123 => 25,  114 => 24,  103 => 21,  97 => 17,  95 => 16,  90 => 14,  85 => 13,  76 => 12,  63 => 9,  57 => 6,  54 => 5,  51 => 4,  42 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block head %}
    {% if collector.hasexception %}
        <style>
            {{ render(path('_profiler_exception_css', { token: token })) }}
        </style>
    {% endif %}
    {{ parent() }}
{% endblock %}

{% block menu %}
    <span class=\"label {{ collector.hasexception ? 'label-status-error' : 'disabled' }}\">
        <span class=\"icon\">{{ include('@WebProfiler/Icon/exception.svg') }}</span>
        <strong>Exception</strong>
        {% if collector.hasexception %}
            <span class=\"count\">
                <span>1</span>
            </span>
        {% endif %}
    </span>
{% endblock %}

{% block panel %}
    <h2>Exceptions</h2>

    {% if not collector.hasexception %}
        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    {% else %}
        <div class=\"sf-reset\">
            {{ render(path('_profiler_exception', { token: token })) }}
        </div>
    {% endif %}
{% endblock %}
", "@WebProfiler/Collector/exception.html.twig", "/home/sj/PhpstormProjects/symfony4-project/vendor/symfony/web-profiler-bundle/Resources/views/Collector/exception.html.twig");
    }
}
