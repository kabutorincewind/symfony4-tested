<?php

/* @WebProfiler/Icon/close.svg */
class __TwigTemplate_d9e0a81e183b7d2ac53eb19058b3dfb480834b3fe0d259fc9625638385ac598d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0debe96d740a34667566b6ffe764c1217d1eb095c4ba15ad5884c1715828524a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0debe96d740a34667566b6ffe764c1217d1eb095c4ba15ad5884c1715828524a->enter($__internal_0debe96d740a34667566b6ffe764c1217d1eb095c4ba15ad5884c1715828524a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Icon/close.svg"));

        $__internal_4ae6a33fc427c268ba078cab1faa71aab33fdf68b9696c220d0c3a52940aa511 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4ae6a33fc427c268ba078cab1faa71aab33fdf68b9696c220d0c3a52940aa511->enter($__internal_4ae6a33fc427c268ba078cab1faa71aab33fdf68b9696c220d0c3a52940aa511_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Icon/close.svg"));

        // line 1
        echo "<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" x=\"0px\" y=\"0px\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" enable-background=\"new 0 0 24 24\" xml:space=\"preserve\">
<path fill=\"#AAAAAA\" d=\"M21.1,18.3c0.8,0.8,0.8,2,0,2.8c-0.4,0.4-0.9,0.6-1.4,0.6s-1-0.2-1.4-0.6L12,14.8l-6.3,6.3
    c-0.4,0.4-0.9,0.6-1.4,0.6s-1-0.2-1.4-0.6c-0.8-0.8-0.8-2,0-2.8L9.2,12L2.9,5.7c-0.8-0.8-0.8-2,0-2.8c0.8-0.8,2-0.8,2.8,0L12,9.2
    l6.3-6.3c0.8-0.8,2-0.8,2.8,0c0.8,0.8,0.8,2,0,2.8L14.8,12L21.1,18.3z\"/>
</svg>
";
        
        $__internal_0debe96d740a34667566b6ffe764c1217d1eb095c4ba15ad5884c1715828524a->leave($__internal_0debe96d740a34667566b6ffe764c1217d1eb095c4ba15ad5884c1715828524a_prof);

        
        $__internal_4ae6a33fc427c268ba078cab1faa71aab33fdf68b9696c220d0c3a52940aa511->leave($__internal_4ae6a33fc427c268ba078cab1faa71aab33fdf68b9696c220d0c3a52940aa511_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Icon/close.svg";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" x=\"0px\" y=\"0px\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" enable-background=\"new 0 0 24 24\" xml:space=\"preserve\">
<path fill=\"#AAAAAA\" d=\"M21.1,18.3c0.8,0.8,0.8,2,0,2.8c-0.4,0.4-0.9,0.6-1.4,0.6s-1-0.2-1.4-0.6L12,14.8l-6.3,6.3
    c-0.4,0.4-0.9,0.6-1.4,0.6s-1-0.2-1.4-0.6c-0.8-0.8-0.8-2,0-2.8L9.2,12L2.9,5.7c-0.8-0.8-0.8-2,0-2.8c0.8-0.8,2-0.8,2.8,0L12,9.2
    l6.3-6.3c0.8-0.8,2-0.8,2.8,0c0.8,0.8,0.8,2,0,2.8L14.8,12L21.1,18.3z\"/>
</svg>
", "@WebProfiler/Icon/close.svg", "/home/sj/PhpstormProjects/symfony4-project/vendor/symfony/web-profiler-bundle/Resources/views/Icon/close.svg");
    }
}
