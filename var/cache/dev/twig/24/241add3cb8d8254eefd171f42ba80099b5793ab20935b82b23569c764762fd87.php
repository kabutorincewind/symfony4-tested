<?php

/* @WebProfiler/Collector/ajax.html.twig */
class __TwigTemplate_b605f6989e0a2287e58c48db5e01347a429a8f255c56ef8cab43042adaabfba2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/ajax.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5950590acf5a0b2530e25397784de6c99cb76ceeef8f820b79454adc9c86724c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5950590acf5a0b2530e25397784de6c99cb76ceeef8f820b79454adc9c86724c->enter($__internal_5950590acf5a0b2530e25397784de6c99cb76ceeef8f820b79454adc9c86724c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/ajax.html.twig"));

        $__internal_7e36a6a6349982b450c6b51df1f7c2e28e108a3646136cd9a00dabeb179a4a3e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7e36a6a6349982b450c6b51df1f7c2e28e108a3646136cd9a00dabeb179a4a3e->enter($__internal_7e36a6a6349982b450c6b51df1f7c2e28e108a3646136cd9a00dabeb179a4a3e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/ajax.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_5950590acf5a0b2530e25397784de6c99cb76ceeef8f820b79454adc9c86724c->leave($__internal_5950590acf5a0b2530e25397784de6c99cb76ceeef8f820b79454adc9c86724c_prof);

        
        $__internal_7e36a6a6349982b450c6b51df1f7c2e28e108a3646136cd9a00dabeb179a4a3e->leave($__internal_7e36a6a6349982b450c6b51df1f7c2e28e108a3646136cd9a00dabeb179a4a3e_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_54d8e512b6ebb8b69a3fc2e52a768fcb5dba3f804abbfaec2806d78f7553782e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_54d8e512b6ebb8b69a3fc2e52a768fcb5dba3f804abbfaec2806d78f7553782e->enter($__internal_54d8e512b6ebb8b69a3fc2e52a768fcb5dba3f804abbfaec2806d78f7553782e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        $__internal_099547bcd5fb5e465bc4618eb72859459476c69039b72f249f04dc99f95ec9bf = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_099547bcd5fb5e465bc4618eb72859459476c69039b72f249f04dc99f95ec9bf->enter($__internal_099547bcd5fb5e465bc4618eb72859459476c69039b72f249f04dc99f95ec9bf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        // line 4
        echo "    ";
        ob_start();
        // line 5
        echo "        ";
        echo twig_include($this->env, $context, "@WebProfiler/Icon/ajax.svg");
        echo "
        <span class=\"sf-toolbar-value sf-toolbar-ajax-request-counter\">0</span>
    ";
        $context["icon"] = ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
        // line 8
        echo "
    ";
        // line 9
        $context["text"] = ('' === $tmp = "        <div class=\"sf-toolbar-info-piece\">
            <b class=\"sf-toolbar-ajax-info\"></b>
        </div>
        <div class=\"sf-toolbar-info-piece\">
            <table class=\"sf-toolbar-ajax-requests\">
                <thead>
                    <tr>
                        <th>Method</th>
                        <th>Type</th>
                        <th>Status</th>
                        <th>URL</th>
                        <th>Time</th>
                        <th>Profile</th>
                    </tr>
                </thead>
                <tbody class=\"sf-toolbar-ajax-request-list\"></tbody>
            </table>
        </div>
    ") ? '' : new Twig_Markup($tmp, $this->env->getCharset());
        // line 29
        echo "
    ";
        // line 30
        echo twig_include($this->env, $context, "@WebProfiler/Profiler/toolbar_item.html.twig", array("link" => false));
        echo "
";
        
        $__internal_099547bcd5fb5e465bc4618eb72859459476c69039b72f249f04dc99f95ec9bf->leave($__internal_099547bcd5fb5e465bc4618eb72859459476c69039b72f249f04dc99f95ec9bf_prof);

        
        $__internal_54d8e512b6ebb8b69a3fc2e52a768fcb5dba3f804abbfaec2806d78f7553782e->leave($__internal_54d8e512b6ebb8b69a3fc2e52a768fcb5dba3f804abbfaec2806d78f7553782e_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/ajax.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  85 => 30,  82 => 29,  62 => 9,  59 => 8,  52 => 5,  49 => 4,  40 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block toolbar %}
    {% set icon %}
        {{ include('@WebProfiler/Icon/ajax.svg') }}
        <span class=\"sf-toolbar-value sf-toolbar-ajax-request-counter\">0</span>
    {% endset %}

    {% set text %}
        <div class=\"sf-toolbar-info-piece\">
            <b class=\"sf-toolbar-ajax-info\"></b>
        </div>
        <div class=\"sf-toolbar-info-piece\">
            <table class=\"sf-toolbar-ajax-requests\">
                <thead>
                    <tr>
                        <th>Method</th>
                        <th>Type</th>
                        <th>Status</th>
                        <th>URL</th>
                        <th>Time</th>
                        <th>Profile</th>
                    </tr>
                </thead>
                <tbody class=\"sf-toolbar-ajax-request-list\"></tbody>
            </table>
        </div>
    {% endset %}

    {{ include('@WebProfiler/Profiler/toolbar_item.html.twig', { link: false }) }}
{% endblock %}
", "@WebProfiler/Collector/ajax.html.twig", "/home/sj/PhpstormProjects/symfony4-project/vendor/symfony/web-profiler-bundle/Resources/views/Collector/ajax.html.twig");
    }
}
