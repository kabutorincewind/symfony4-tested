<?php

/* @WebProfiler/Collector/router.html.twig */
class __TwigTemplate_d6c5a1024cb396528d6531e5b3ecd7041815765b0f8899d0570a5a0cec35b659 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_628a0fccea10287faf9ae3250b253adb54a061ce290bf4f9828466c34f8d4244 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_628a0fccea10287faf9ae3250b253adb54a061ce290bf4f9828466c34f8d4244->enter($__internal_628a0fccea10287faf9ae3250b253adb54a061ce290bf4f9828466c34f8d4244_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $__internal_364a2bb5c96d94a6ccf05041fcd392cf9afd09bb2975a7a549b265919ea18b18 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_364a2bb5c96d94a6ccf05041fcd392cf9afd09bb2975a7a549b265919ea18b18->enter($__internal_364a2bb5c96d94a6ccf05041fcd392cf9afd09bb2975a7a549b265919ea18b18_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_628a0fccea10287faf9ae3250b253adb54a061ce290bf4f9828466c34f8d4244->leave($__internal_628a0fccea10287faf9ae3250b253adb54a061ce290bf4f9828466c34f8d4244_prof);

        
        $__internal_364a2bb5c96d94a6ccf05041fcd392cf9afd09bb2975a7a549b265919ea18b18->leave($__internal_364a2bb5c96d94a6ccf05041fcd392cf9afd09bb2975a7a549b265919ea18b18_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_3cdb5e8c708bc333c22b3f8f499446674933722810e511e3732e4270be165d60 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3cdb5e8c708bc333c22b3f8f499446674933722810e511e3732e4270be165d60->enter($__internal_3cdb5e8c708bc333c22b3f8f499446674933722810e511e3732e4270be165d60_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        $__internal_8e4aaa4b722db7331063abfb3750dbbdfe5df6fc6d3d344ec8ab2535948ef711 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8e4aaa4b722db7331063abfb3750dbbdfe5df6fc6d3d344ec8ab2535948ef711->enter($__internal_8e4aaa4b722db7331063abfb3750dbbdfe5df6fc6d3d344ec8ab2535948ef711_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_8e4aaa4b722db7331063abfb3750dbbdfe5df6fc6d3d344ec8ab2535948ef711->leave($__internal_8e4aaa4b722db7331063abfb3750dbbdfe5df6fc6d3d344ec8ab2535948ef711_prof);

        
        $__internal_3cdb5e8c708bc333c22b3f8f499446674933722810e511e3732e4270be165d60->leave($__internal_3cdb5e8c708bc333c22b3f8f499446674933722810e511e3732e4270be165d60_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_009499914128e19b06211585b23267d6c5da807b5ff6dc9c40f4c7f87e178c78 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_009499914128e19b06211585b23267d6c5da807b5ff6dc9c40f4c7f87e178c78->enter($__internal_009499914128e19b06211585b23267d6c5da807b5ff6dc9c40f4c7f87e178c78_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_e74af12af49636e58068a364b32cb450e3005a198c7ee85076c37c99d93ce7a2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e74af12af49636e58068a364b32cb450e3005a198c7ee85076c37c99d93ce7a2->enter($__internal_e74af12af49636e58068a364b32cb450e3005a198c7ee85076c37c99d93ce7a2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_e74af12af49636e58068a364b32cb450e3005a198c7ee85076c37c99d93ce7a2->leave($__internal_e74af12af49636e58068a364b32cb450e3005a198c7ee85076c37c99d93ce7a2_prof);

        
        $__internal_009499914128e19b06211585b23267d6c5da807b5ff6dc9c40f4c7f87e178c78->leave($__internal_009499914128e19b06211585b23267d6c5da807b5ff6dc9c40f4c7f87e178c78_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_552df2bb859066dc90d0e87b95cdd9c05e823196275ba48a7074f1bf6e9b7e10 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_552df2bb859066dc90d0e87b95cdd9c05e823196275ba48a7074f1bf6e9b7e10->enter($__internal_552df2bb859066dc90d0e87b95cdd9c05e823196275ba48a7074f1bf6e9b7e10_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_db7ee8dae307cc0ac171af17d5e16081ce17e3bdaafc351db863d45cb62861e6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_db7ee8dae307cc0ac171af17d5e16081ce17e3bdaafc351db863d45cb62861e6->enter($__internal_db7ee8dae307cc0ac171af17d5e16081ce17e3bdaafc351db863d45cb62861e6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_router", array("token" => (isset($context["token"]) || array_key_exists("token", $context) ? $context["token"] : (function () { throw new Twig_Error_Runtime('Variable "token" does not exist.', 13, $this->getSourceContext()); })()))));
        echo "
";
        
        $__internal_db7ee8dae307cc0ac171af17d5e16081ce17e3bdaafc351db863d45cb62861e6->leave($__internal_db7ee8dae307cc0ac171af17d5e16081ce17e3bdaafc351db863d45cb62861e6_prof);

        
        $__internal_552df2bb859066dc90d0e87b95cdd9c05e823196275ba48a7074f1bf6e9b7e10->leave($__internal_552df2bb859066dc90d0e87b95cdd9c05e823196275ba48a7074f1bf6e9b7e10_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  94 => 13,  85 => 12,  71 => 7,  68 => 6,  59 => 5,  42 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block toolbar %}{% endblock %}

{% block menu %}
<span class=\"label\">
    <span class=\"icon\">{{ include('@WebProfiler/Icon/router.svg') }}</span>
    <strong>Routing</strong>
</span>
{% endblock %}

{% block panel %}
    {{ render(path('_profiler_router', { token: token })) }}
{% endblock %}
", "@WebProfiler/Collector/router.html.twig", "/home/sj/PhpstormProjects/symfony4-project/vendor/symfony/web-profiler-bundle/Resources/views/Collector/router.html.twig");
    }
}
