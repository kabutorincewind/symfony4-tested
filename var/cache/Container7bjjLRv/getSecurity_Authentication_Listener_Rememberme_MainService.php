<?php

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.
// Returns the private 'security.authentication.listener.rememberme.main' shared service.

include_once $this->targetDirs[2].'/vendor/symfony/security/Http/Session/SessionAuthenticationStrategyInterface.php';
include_once $this->targetDirs[2].'/vendor/symfony/security/Http/Session/SessionAuthenticationStrategy.php';
include_once $this->targetDirs[2].'/vendor/symfony/security/Http/Firewall/ListenerInterface.php';
include_once $this->targetDirs[2].'/vendor/symfony/security/Http/Firewall/RememberMeListener.php';

return $this->privates['security.authentication.listener.rememberme.main'] = new \Symfony\Component\Security\Http\Firewall\RememberMeListener(($this->services['security.token_storage'] ?? $this->services['security.token_storage'] = new \Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage()), ($this->privates['security.authentication.rememberme.services.simplehash.main'] ?? $this->load(__DIR__.'/getSecurity_Authentication_Rememberme_Services_Simplehash_MainService.php')), ($this->privates['security.authentication.manager'] ?? $this->getSecurity_Authentication_ManagerService()), ($this->privates['monolog.logger.security'] ?? $this->load(__DIR__.'/getMonolog_Logger_SecurityService.php')), ($this->services['event_dispatcher'] ?? $this->getEventDispatcherService()), true, new \Symfony\Component\Security\Http\Session\SessionAuthenticationStrategy('migrate'));
