<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class srcDebugProjectContainerUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($rawPathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($rawPathinfo);
        $trimmedPathinfo = rtrim($pathinfo, '/');
        $context = $this->context;
        $request = $this->request;
        $requestMethod = $canonicalMethod = $context->getMethod();
        $scheme = $context->getScheme();

        if ('HEAD' === $requestMethod) {
            $canonicalMethod = 'GET';
        }


        // security_login
        if ('/login' === $pathinfo) {
            return array (  '_controller' => 'App\\Controller\\SecurityController::login',  '_route' => 'security_login',);
        }

        // security_logout
        if ('/logout' === $pathinfo) {
            return array (  '_controller' => 'App\\Controller\\SecurityController::logoutAction',  '_route' => 'security_logout',);
        }

        // homepage
        if ('' === $trimmedPathinfo) {
            $ret = array (  '_controller' => 'App\\Controller\\SiteController::homepage',  '_route' => 'homepage',);
            if (substr($pathinfo, -1) !== '/') {
                return array_replace($ret, $this->redirect($rawPathinfo.'/', 'homepage'));
            }

            return $ret;
        }

        // statistics
        if ('/statistics' === $pathinfo) {
            return array (  '_controller' => 'App\\Controller\\SiteController::statistics',  '_route' => 'statistics',);
        }

        if (0 === strpos($pathinfo, '/user')) {
            if (0 === strpos($pathinfo, '/user/re')) {
                // user_register
                if ('/user/register' === $pathinfo) {
                    return array (  '_controller' => 'App\\Controller\\UserController::register',  '_route' => 'user_register',);
                }

                // user_request_password_reset
                if ('/user/request-password-reset' === $pathinfo) {
                    return array (  '_controller' => 'App\\Controller\\UserController::requestPasswordReset',  '_route' => 'user_request_password_reset',);
                }

                // user_reset_password
                if (0 === strpos($pathinfo, '/user/reset-password') && preg_match('#^/user/reset\\-password/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'user_reset_password')), array (  '_controller' => 'App\\Controller\\UserController::resetPassword',));
                }

            }

            // user_activate
            if (0 === strpos($pathinfo, '/user/activate') && preg_match('#^/user/activate/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'user_activate')), array (  '_controller' => 'App\\Controller\\UserController::activate',));
            }

            // user_edit
            if ('/user/edit' === $pathinfo) {
                return array (  '_controller' => 'App\\Controller\\UserController::edit',  '_route' => 'user_edit',);
            }

        }

        // dependent_select
        if ('/dependent_select' === $pathinfo) {
            return array (  '_controller' => 'DependentSelectBundle:DependentFilteredEntity:getOptions',  '_route' => 'dependent_select',);
        }

        // _alpha_device_fingerprint
        if ('/device/fingerprint' === $pathinfo) {
            return array (  '_controller' => 'Alpha\\VisitorTrackingBundle\\Controller\\DeviceController::fingerprintAction',  '_route' => '_alpha_device_fingerprint',);
        }

        // index
        if ('' === $trimmedPathinfo) {
            $ret = array (  '_controller' => 'App\\Controller\\DefaultController::index',  '_route' => 'index',);
            if (substr($pathinfo, -1) !== '/') {
                return array_replace($ret, $this->redirect($rawPathinfo.'/', 'index'));
            }

            return $ret;
        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
