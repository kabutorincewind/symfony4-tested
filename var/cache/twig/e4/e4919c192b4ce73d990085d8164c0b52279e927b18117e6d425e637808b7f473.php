<?php

/* @Twig/layout.html.twig */
class __TwigTemplate_c859c85214c51f0544f4b0315cf9e3bc24e8f65e698c6d3d27f18f4ff28c7f7c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'head' => array($this, 'block_head'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6a483939021aa68ea644190a3afaa442ac134efc7f6dd2e31c539a98ca133733 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6a483939021aa68ea644190a3afaa442ac134efc7f6dd2e31c539a98ca133733->enter($__internal_6a483939021aa68ea644190a3afaa442ac134efc7f6dd2e31c539a98ca133733_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/layout.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getCharset(), "html", null, true);
        echo "\" />
        <meta name=\"robots\" content=\"noindex,nofollow\" />
        <meta name=\"viewport\" content=\"width=device-width,initial-scale=1\" />
        <title>";
        // line 7
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        <link rel=\"icon\" type=\"image/png\" href=\"";
        // line 8
        echo twig_include($this->env, $context, "@Twig/images/favicon.png.base64");
        echo "\">
        <style>";
        // line 9
        echo twig_include($this->env, $context, "@Twig/exception.css.twig");
        echo "</style>
        ";
        // line 10
        $this->displayBlock('head', $context, $blocks);
        // line 11
        echo "    </head>
    <body>
        <header>
            <div class=\"container\">
                <h1 class=\"logo\">";
        // line 15
        echo twig_include($this->env, $context, "@Twig/images/symfony-logo.svg");
        echo " Symfony Exception</h1>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/doc\">
                        <span class=\"icon\">";
        // line 19
        echo twig_include($this->env, $context, "@Twig/images/icon-book.svg");
        echo "</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Docs
                    </a>
                </div>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/support\">
                        <span class=\"icon\">";
        // line 26
        echo twig_include($this->env, $context, "@Twig/images/icon-support.svg");
        echo "</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Support
                    </a>
                </div>
            </div>
        </header>

        ";
        // line 33
        $this->displayBlock('body', $context, $blocks);
        // line 34
        echo "        ";
        echo twig_include($this->env, $context, "@Twig/base_js.html.twig");
        echo "
    </body>
</html>
";
        
        $__internal_6a483939021aa68ea644190a3afaa442ac134efc7f6dd2e31c539a98ca133733->leave($__internal_6a483939021aa68ea644190a3afaa442ac134efc7f6dd2e31c539a98ca133733_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_4e15ba66097798587f6094618c8b91a28596d442c3950adb8558634b6607a5ac = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4e15ba66097798587f6094618c8b91a28596d442c3950adb8558634b6607a5ac->enter($__internal_4e15ba66097798587f6094618c8b91a28596d442c3950adb8558634b6607a5ac_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        
        $__internal_4e15ba66097798587f6094618c8b91a28596d442c3950adb8558634b6607a5ac->leave($__internal_4e15ba66097798587f6094618c8b91a28596d442c3950adb8558634b6607a5ac_prof);

    }

    // line 10
    public function block_head($context, array $blocks = array())
    {
        $__internal_f43e53941f2157df1019e1b2ebfa241946672ec99e5b79f1bd9bac19256c9f6c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f43e53941f2157df1019e1b2ebfa241946672ec99e5b79f1bd9bac19256c9f6c->enter($__internal_f43e53941f2157df1019e1b2ebfa241946672ec99e5b79f1bd9bac19256c9f6c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        
        $__internal_f43e53941f2157df1019e1b2ebfa241946672ec99e5b79f1bd9bac19256c9f6c->leave($__internal_f43e53941f2157df1019e1b2ebfa241946672ec99e5b79f1bd9bac19256c9f6c_prof);

    }

    // line 33
    public function block_body($context, array $blocks = array())
    {
        $__internal_2a4364465820002a64a2f0f94048e9b95dc92683dfb242c8940327911865840c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2a4364465820002a64a2f0f94048e9b95dc92683dfb242c8940327911865840c->enter($__internal_2a4364465820002a64a2f0f94048e9b95dc92683dfb242c8940327911865840c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_2a4364465820002a64a2f0f94048e9b95dc92683dfb242c8940327911865840c->leave($__internal_2a4364465820002a64a2f0f94048e9b95dc92683dfb242c8940327911865840c_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  119 => 33,  108 => 10,  97 => 7,  85 => 34,  83 => 33,  73 => 26,  63 => 19,  56 => 15,  50 => 11,  48 => 10,  44 => 9,  40 => 8,  36 => 7,  30 => 4,  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"{{ _charset }}\" />
        <meta name=\"robots\" content=\"noindex,nofollow\" />
        <meta name=\"viewport\" content=\"width=device-width,initial-scale=1\" />
        <title>{% block title %}{% endblock %}</title>
        <link rel=\"icon\" type=\"image/png\" href=\"{{ include('@Twig/images/favicon.png.base64') }}\">
        <style>{{ include('@Twig/exception.css.twig') }}</style>
        {% block head %}{% endblock %}
    </head>
    <body>
        <header>
            <div class=\"container\">
                <h1 class=\"logo\">{{ include('@Twig/images/symfony-logo.svg') }} Symfony Exception</h1>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/doc\">
                        <span class=\"icon\">{{ include('@Twig/images/icon-book.svg') }}</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Docs
                    </a>
                </div>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/support\">
                        <span class=\"icon\">{{ include('@Twig/images/icon-support.svg') }}</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Support
                    </a>
                </div>
            </div>
        </header>

        {% block body %}{% endblock %}
        {{ include('@Twig/base_js.html.twig') }}
    </body>
</html>
", "@Twig/layout.html.twig", "/home/sj/PhpstormProjects/symfony4-project/vendor/symfony/twig-bundle/Resources/views/layout.html.twig");
    }
}
