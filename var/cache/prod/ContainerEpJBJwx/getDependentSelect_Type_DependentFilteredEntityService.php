<?php

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.
// Returns the private 'dependent_select.type.dependent_filtered_entity' shared service.

include_once $this->targetDirs[3].'/vendor/symfony/form/FormTypeInterface.php';
include_once $this->targetDirs[3].'/vendor/symfony/form/AbstractType.php';
include_once $this->targetDirs[3].'/vendor/kabuto/dependent-select-bundle/Form/Type/DependentFilteredEntityType.php';

return $this->privates['dependent_select.type.dependent_filtered_entity'] = new \Evercode\DependentSelectBundle\Form\Type\DependentFilteredEntityType($this);
