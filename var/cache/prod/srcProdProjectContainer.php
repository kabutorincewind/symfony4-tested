<?php

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.

if (\class_exists(\ContainerEpJBJwx\srcProdProjectContainer::class, false)) {
    // no-op
} elseif (!include __DIR__.'/ContainerEpJBJwx/srcProdProjectContainer.php') {
    touch(__DIR__.'/ContainerEpJBJwx.legacy');

    return;
}

if (!\class_exists(srcProdProjectContainer::class, false)) {
    \class_alias(\ContainerEpJBJwx\srcProdProjectContainer::class, srcProdProjectContainer::class, false);
}

return new \ContainerEpJBJwx\srcProdProjectContainer(array(
    'container.build_hash' => 'EpJBJwx',
    'container.build_id' => '12b82767',
    'container.build_time' => 1517257184,
));
