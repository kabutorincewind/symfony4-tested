require('../css/app.scss');

var $ = require('jquery');
window.$ = $;
window.jQuery = $;
window.Popper = require('popper.js');

// create global $ and jQuery variables
global.$ = global.jQuery = $;

require('bootstrap');

var _ = require('lodash');
_.templateSettings.interpolate = /\<\{([\s\S]+?)\}\>/g;
_.templateSettings.evaluate = /\<%([\s\S]+?)%\>/g;

